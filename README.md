# README #

THESE ARE STILL A WORK IN PROGRESS.........




There are three demo containers in this repository:  PEM 6.0, xDB 6.0, and PPAS 9.5

### xDB 6.0 ###

This image will create a container with two PG master clusers configured for MMR with xDB 6.  It will start the clusters, start replication, and also launch a PEM client and xDB replication console for demo purposes.

The two PG clusters run on ports 5445 and 5446.  The admin user is enterprisedb with password enterprisedb.  The three tables configured for replication are in the public schema of the postgres database on each cluster.

The following commands assume you have changed to the xdb directory:

To build:

```
#!bash

make buildImage

```

To run:

```
#!bash

make runConsole

```

### PEM 6.0 ###

This image will create a container with PEM 6.0 server installed with an EPAS 9.5 backend.  Username/passwords are enterprisedb/enterprisedb.  There are makefile targets to launch the thin client GUI or the thick client GUI for the demo.

The following commands assume you have changed to the pem directory:

To build:

```
#!bash

make buildImage

```

To run the container and launch the PEM thick client:

```
#!bash

make runThick

```
To run the container and launch the PEM browser client:

```
#!bash

make runThin

```
### EPAS 9.5 ###

This image is still in early beta and should probably not be used yet.
