#########################################################################
# Setup all of the certificates.  Below we create a CA,
# which you may not need if you are using an external CA.
#########################################################################
mkdir ~/ca 
cd ~/ca 

# Create ca.conf file
cat <<EOF > ca.conf
[ ca ]
default_ca = ca_default
[ ca_default ]
dir = /var/lib/ppas/ca
certs = \$dir
new_certs_dir =\$dir/ca.db.certs
database = \$dir/ca.db.index
serial = \$dir/ca.db.serial
RANDFILE = \$dir/ca.db.rand
certificate = \$dir/ca.crt
private_key = \$dir/ca.key
default_days = 365
default_crl_days = 30
default_md = sha1
preserve = no 
policy = generic_policy
[ generic_policy ]
countryName = optional
stateOrProvinceName = optional
localityName = optional
organizationName = optional
organizationalUnitName = optional
commonName = optional
emailAddress = optional
EOF

# Create some initial files 
mkdir ca.db.certs 
touch ca.db.index 
echo "0001" > ca.db.serial  
 
# Generate the CA cert and put in keystore (the truststore)
openssl genrsa 1024 > ca.key 
openssl req -new -x509 -sha1 -key ca.key -out ca.crt -subj "/C=US/ST=Teas/L=Dallas/O=My CA/OU=My Department/CN=my-ca.com" 
openssl x509 -in ca.crt -out ca.der -outform der
keytool -import -noprompt -keystore ca.keystore -storepass "ca-key-pass" -alias castore -file ca.der

# Generate a cert for the first master DB server
openssl genrsa 1024 > master1.key 
openssl req -new -sha1 -key master1.key -out master1.csr -subj "/C=US/ST=Teas/L=Dallas/O=My Company/OU=Master1/CN=localhost" 
openssl ca -batch -md sha1 -config ca.conf -out master1.crt -infiles master1.csr  

# Generate a cert for the second master DB server 
openssl genrsa 1024 > master2.key 
openssl req -new -sha1 -key master2.key -out master2.csr -subj "/C=US/ST=Teas/L=Dallas/O=My Company/OU=Master2/CN=localhost" 
openssl ca -batch -md sha1 -config ca.conf -out master2.crt -infiles master2.csr  

# Generate a cert and JKS for the enterprisedb user
openssl genrsa 1024 > enterprisedb.key 
openssl req -new -sha1 -key enterprisedb.key -out enterprisedb.csr -subj "/C=US/ST=Teas/L=Dallas/O=My Company/OU=My Department/CN=enterprisedb" 
openssl ca -batch -md sha1 -config ca.conf -out enterprisedb.crt -infiles enterprisedb.csr 
openssl pkcs12 -export -passout pass:enterprisedb-key-pass  -in enterprisedb.crt -inkey enterprisedb.key > enterprisedb.p12 
keytool -importkeystore -srckeystore enterprisedb.p12 -destkeystore enterprisedb.jks -srcstoretype pkcs12 -destkeypass "enterprisedb-key-pass"  -deststorepass "enterprisedb-key-pass" -srcstorepass "enterprisedb-key-pass" 

# Generate a cert and JKS for a repl_user
openssl genrsa 1024 > repl_user.key 
openssl req -new -sha1 -key repl_user.key -out repl_user.csr -subj "/C=US/ST=Teas/L=Dallas/O=My Company/OU=My Department/CN=repl_user" 
openssl ca -batch -md sha1 -config ca.conf -out repl_user.crt -infiles repl_user.csr 
openssl pkcs12 -export -passout pass:repl-key-pass  -in repl_user.crt -inkey repl_user.key > repl_user.p12 
keytool -importkeystore -srckeystore repl_user.p12 -destkeystore repl_user.jks -srcstoretype pkcs12 -destkeypass "repl-key-pass"  -deststorepass "repl-key-pass" -srcstorepass "repl-key-pass" 

# Generate an initial CRL
openssl ca -config ca.conf -gencrl -keyfile ca.key -cert ca.crt -out ca.crl 

#######################################################################
# Create two master clusters, master1 on 5444 and master2 on 5445
#
# Currently doing this in the Docker file because they are slow while
# debugging this script.  Once debugged, will do these here instead of
# in the Dockerfile.
#######################################################################
#mkdir /var/lib/ppas/9.5/master1 
#mkdir /var/lib/ppas/9.5/master2 
#chmod 700 /var/lib/ppas/9.5/master* 
#/usr/ppas-9.5/bin/initdb -D /var/lib/ppas/9.5/master1 
#/usr/ppas-9.5/bin/initdb -D /var/lib/ppas/9.5/master2 
#sed -i 's/#port = 5444/port = 5445/' /var/lib/ppas/9.5/master2/postgresql.conf

#######################################################################
# Setup two way SSL to be required by all IPV4 and IPV6 connections to both servers
#######################################################################
echo "hostssl all all 0.0.0.0/0 cert clientcert=1" > /var/lib/ppas/9.5/master1/pg_hba.conf 
echo "hostssl all all ::0/0 cert clientcert=1" >> /var/lib/ppas/9.5/master1/pg_hba.conf
echo "hostssl all all 0.0.0.0/0 cert clientcert=1" > /var/lib/ppas/9.5/master2/pg_hba.conf 
echo "hostssl all all ::0/0 cert clientcert=1" >> /var/lib/ppas/9.5/master2/pg_hba.conf

#######################################################################
# Configure the necessary settings to permit the clusters to stream logs to the Replication Server
#######################################################################
sed -i 's/#wal_level = minimal/wal_level = logical/' /var/lib/ppas/9.5/master*/postgresql.conf 
sed -i 's/#max_wal_senders = 0/max_wal_senders = 5/' /var/lib/ppas/9.5/master*/postgresql.conf 
sed -i 's/#max_replication_slots = 0/max_replication_slots = 5/' /var/lib/ppas/9.5/master*/postgresql.conf 
sed -i 's/#track_commit_timestamp = off/track_commit_timestamp = on/' /var/lib/ppas/9.5/master*/postgresql.conf 
sed -i 's/#host[ ]*replication/host replication/g' /var/lib/ppas/9.5/master*/pg_hba.conf

#######################################################################
# Copy CA and server certs to PGDATA dirs 
#######################################################################
cp ~/ca/ca.crt ~/ca/ca.crl ~/ca/master1.key ~/ca/master1.crt /var/lib/ppas/9.5/master1
cp ~/ca/ca.crt ~/ca/ca.crl ~/ca/master2.key ~/ca/master2.crt /var/lib/ppas/9.5/master2
chmod 600 /var/lib/ppas/9.5/master*/*.key 
chmod 600 /var/lib/ppas/9.5/master*/*.crt 
chmod 600 /var/lib/ppas/9.5/master*/*.crl

#######################################################################
# Setup both PGDATA clusters for SSL
#######################################################################
sed -i "s/#logging_collector = off/logging_collector = on/"               ~/9.5/master*/postgresql.conf 
sed -i "s/#ssl = off/ssl = on/"                                           ~/9.5/master*/postgresql.conf 
sed -i "s/#ssl_ca_file = ''/ssl_ca_file = 'ca.crt'/"                      ~/9.5/master*/postgresql.conf 
sed -i "s/#ssl_crl_file = ''/ssl_crl_file = 'ca.crl'/"                    ~/9.5/master*/postgresql.conf 

sed -i "s/#ssl_cert_file = 'server.crt'/ssl_cert_file = 'master1.crt'/" ~/9.5/master1/postgresql.conf 
sed -i "s/#ssl_key_file = 'server.key'/ssl_key_file = 'master1.key'/"   ~/9.5/master1/postgresql.conf 
sed -i "s/#ssl_cert_file = 'server.crt'/ssl_cert_file = 'master2.crt'/" ~/9.5/master2/postgresql.conf 
sed -i "s/#ssl_key_file = 'server.key'/ssl_key_file = 'master2.key'/"   ~/9.5/master2/postgresql.conf 

#######################################################################
# Copy keys/certs to enterprisedb's home director
#######################################################################
mkdir ~/.postgresql 
cp ~/ca/enterprisedb.crt  ~/.postgresql/postgresql.crt 
cp ~/ca/enterprisedb.key  ~/.postgresql/postgresql.key 
cp ~/ca/enterprisedb.jks  ~/.postgresql/enterprisedb.jks 
cp ~/ca/ca.keystore       ~/.postgresql/ca.keystore 
cp ~/ca/enterprisedb.p12  ~/.postgresql/enterprisedb.p12 
cp ~/ca/repl_user.p12     ~/.postgresql/repl_user.p12 
cp ~/ca/repl_user.crt     ~/.postgresql/repl_user.crt 
cp ~/ca/repl_user.key     ~/.postgresql/repl_user.key 
chmod 600 ~/.postgresql/*

#######################################################################
# Get xDB encrypted values for the certain passwords
#######################################################################
echo 'enterprisedb' > ~/enterprisedb_pass.in
echo 'ca-key-pass' > ~/ca_keypass.in
echo 'enterprisedb-key-pass' > ~/enterprisedb_keypass.in
echo 'repl-key-pass' > ~/repl_keypass.in
echo 'dummy-pass' > ~/dummy_pass.in
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -encrypt -input ~/enterprisedb_pass.in -output ~/enterprisedb_pass.out
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -encrypt -input ~/ca_keypass.in -output ~/ca_keypass.out
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -encrypt -input ~/enterprisedb_keypass.in -output ~/enterprisedb_keypass.out
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -encrypt -input ~/repl_keypass.in -output ~/repl_keypass.out
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -encrypt -input ~/dummy_pass.in -output ~/dummy_pass.out

#######################################################################
# Create the /etc/edb-repl.conf main configuration file
#######################################################################
cat <<EOF > /etc/edb-repl.conf
admin_user=enterprisedb
admin_password=`cat ~enterprisedb/enterprisedb_pass.out`
user=repl_user
host=localhost
port=5444
database=edb?ssl=true
EOF

#######################################################################
# Setup SSL stores in the pubserver configuration file
#######################################################################
echo "sslTrustStore=/var/lib/ppas/.postgresql/ca.keystore" >> /usr/ppas-xdb-6.0/etc/xdb_pubserver.conf
echo "sslTrustStorePassword=`cat ~enterprisedb/ca_keypass.out`" >> /usr/ppas-xdb-6.0/etc/xdb_pubserver.conf
echo "sslKeyStore=/var/lib/ppas/.postgresql/repl_user.p12" >> /usr/ppas-xdb-6.0/etc/xdb_pubserver.conf
echo "sslKeyStorePassword=`cat ~enterprisedb/repl_keypass.out`" >> /usr/ppas-xdb-6.0/etc/xdb_pubserver.conf

#######################################################################
# Start the master databases 
#######################################################################
/usr/ppas-9.5/bin/pg_ctl -wD /var/lib/ppas/9.5/master1 start
/usr/ppas-9.5/bin/pg_ctl -wD /var/lib/ppas/9.5/master2 start

#######################################################################
# Create the repl_user that will by used by xDB
#######################################################################
psql -d edb -U enterprisedb -p 5444 -h localhost -c "create user repl_user"
psql -d edb -U enterprisedb -p 5444 -h localhost -c "alter user repl_user with superuser"
psql -d edb -U enterprisedb -p 5445 -h localhost -c "create user repl_user"
psql -d edb -U enterprisedb -p 5445 -h localhost -c "alter user repl_user with superuser"

#######################################################################
# Create three tables to be replicated on each cluster
#######################################################################
export PGSSLCERT=~/.postgresql/repl_user.crt
export PGSSLKEY=~/.postgresql/repl_user.key

psql -d edb -U repl_user -p 5444 -h localhost -c "create table public.table1 (id integer primary key)"
psql -d edb -U repl_user -p 5444 -h localhost -c "create table public.table2 (id text primary key)"
psql -d edb -U repl_user -p 5444 -h localhost -c "create table public.table3 (id integer primary key)"

psql -d edb -U repl_user -p 5445 -h localhost -c "create table public.table1 (id integer primary key)"
psql -d edb -U repl_user -p 5445 -h localhost -c "create table public.table2 (id text primary key)"
psql -d edb -U repl_user -p 5445 -h localhost -c "create table public.table3 (id integer primary key)"

unset PGSSLCERT PGSSLKEY

#######################################################################
# Start the publication server
#######################################################################
/usr/ppas-xdb-6.0/bin/runPubServer.sh & sleep 3
PUBSERVER_PID=$!

#######################################################################
# Setup cli_conf.conf for CLI (Command Line Interface) use
#######################################################################
cat <<EOF > ~/cli_conf.conf
user=enterprisedb
password=`cat ~enterprisedb/enterprisedb_pass.out`
host=localhost
port=9051
EOF

#######################################################################
# Add the first master as a publication database (becomes Master Definition Node - MDN).
# The changesetlogmode W is the indication to use WAL streaming instead of triggers.
#######################################################################
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -addpubdb -repsvrfile ~/cli_conf.conf \
   -dbtype enterprisedb -dbhost localhost -dbport 5444 -dbuser repl_user -dbpassword `cat ~/dummy_pass.out` \
   -changesetlogmode W -database edb -urloptions ssl=true \
   -repgrouptype m -nodepriority 1 

#######################################################################
# Get the ID of the newly added MDN
#######################################################################
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -printmdndbid -repsvrfile ~/cli_conf.conf | grep -v 'Printing' > ~/mdn_id.txt 

#######################################################################
# Get a list of the tables to be replicated
#######################################################################
psql -d edb -U enterprisedb -p 5444 -h localhost -c "select '' || table_schema || '.' || table_name from information_schema.tables where table_schema = 'public' and table_name like '%table%'" | grep "\." > ~/tables.txt 

#######################################################################
# Create the publication on the existing master
#######################################################################
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -createpub my_pub -repsvrfile ~/cli_conf.conf -pubdbid `cat ~enterprisedb/mdn_id.txt` -reptype T -tables `cat ~enterprisedb/tables.txt | tr '\n' ' '` -repgrouptype m 

#######################################################################
# Add the second master as a publication database.
# The changesetlogmode W is the indication to use WAL streaming instead of triggers.
#######################################################################
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -addpubdb -repsvrfile ~/cli_conf.conf -dbtype enterprisedb -dbhost localhost -dbport 5445 -dbuser repl_user -changesetlogmode W -dbpassword `cat ~/dummy_pass.out` -database edb -urloptions ssl=true -repgrouptype m -nodepriority 2 -replicatepubschema false

#######################################################################
# Setup a schedule to perform synchronization every 10 seconds
#######################################################################
java -jar /usr/ppas-xdb-6.0/bin/edb-repcli.jar -confschedulemmr `cat ~enterprisedb/mdn_id.txt` -pubname my_pub -repsvrfile ~/cli_conf.conf -realtime 10

#######################################################################
# Stop the pubserver and the databases
#######################################################################
kill $PUBSERVER_PID
sleep 10 
/usr/ppas-9.5/bin/pg_ctl -mf -D /var/lib/ppas/9.5/master1 stop
/usr/ppas-9.5/bin/pg_ctl -mf -D /var/lib/ppas/9.5/master2 stop




