#!/bin/bash -l

#
# Make it easy to login
#
echo "host all all 0.0.0.0/0 trust" >> /var/lib/ppas/9.5/data_master1/pg_hba.conf
echo "host all all 0.0.0.0/0 trust" >> /var/lib/ppas/9.5/data_master2/pg_hba.conf

# Make sure listen_addresses = '*' in postgresql.conf
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /var/lib/ppas/9.5/data_master1/postgresql.conf
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /var/lib/ppas/9.5/data_master2/postgresql.conf

#
# Start the two clusters that will be masters
#
pg_ctl -w -D /var/lib/ppas/9.5/data_master1 -o "-p 5445" start
pg_ctl -w -D /var/lib/ppas/9.5/data_master2 -o "-p 5446" start

#
# Set password for enterprisedb database user
#
psql -d postgres -p 5445 -c "alter role enterprisedb identified by enterprisedb"
psql -d postgres -p 5446 -c "alter role enterprisedb identified by enterprisedb"

#
# Create three tables to be replicated on each cluster
#
psql -p 5445 -c "create table public.table1 (id integer primary key)";
psql -p 5445 -c "create table public.table2 (id text primary key)";
psql -p 5445 -c "create table public.table3 (id integer primary key)";
psql -p 5446 -c "create table public.table1 (id integer primary key)";
psql -p 5446 -c "create table public.table2 (id text primary key)";
psql -p 5446 -c "create table public.table3 (id integer primary key)";

#
# Setup environment to run the CLI commands
#
export XDBHOME=/usr/ppas-xdb-6.0
echo "enterprisedb" > ~/masterpass1.in
echo "enterprisedb" > ~/masterpass2.in
echo "enterprisedb" > ~/adminpass.in

#
# Encrypt passwords
#
java -jar $XDBHOME/bin/edb-repcli.jar -encrypt -input ~/masterpass1.in -output ~/masterpass1.out
java -jar $XDBHOME/bin/edb-repcli.jar -encrypt -input ~/masterpass2.in -output ~/masterpass2.out
java -jar $XDBHOME/bin/edb-repcli.jar -encrypt -input ~/adminpass.in -output ~/adminpass.out

#
# Setup /etc/edb-repl.conf so pub/sub servers can start
#
echo "admin_user=enterprisedb"              >  /etc/edb-repl.conf
echo "admin_password=`cat ~/adminpass.out`" >> /etc/edb-repl.conf
echo "user=enterprisedb"                    >> /etc/edb-repl.conf
echo "password=`cat ~/masterpass1.out`"     >> /etc/edb-repl.conf
echo "host=localhost"                       >> /etc/edb-repl.conf
echo "port=5445"                            >> /etc/edb-repl.conf
echo "database=postgres"                    >> /etc/edb-repl.conf

#
# Setup pubsvrfile.conf for CLI use
#
echo "user=enterprisedb"                    >  ~/pubsvrfile.conf
echo "password=`cat ~/adminpass.out`"       >> ~/pubsvrfile.conf
echo "host=localhost"                       >> ~/pubsvrfile.conf
echo "port=9051"                            >> ~/pubsvrfile.conf

#
# Setup pubsvrfile.conf for CLI use
#
echo "user=enterprisedb"                    >  ~/subsvrfile.conf
echo "password=`cat ~/adminpass.out`"       >> ~/subsvrfile.conf
echo "host=localhost"                       >> ~/subsvrfile.conf
echo "port=9052"                            >> ~/subsvrfile.conf

#
# Start pubserver in background and give a few seconds for it to start
#
$XDBHOME/bin/runPubServer.sh &
sleep 5

#
# Start pubserver in background and give a few seconds for it to start
#
$XDBHOME/bin/runSubServer.sh &
sleep 5

#
# Use CLI to add masters and configure publications
#
java -jar $XDBHOME/bin/edb-repcli.jar -addpubdb -repsvrfile ~/pubsvrfile.conf -dbtype enterprisedb -dbhost localhost -dbport 5445 -dbuser enterprisedb -dbpassfile ~/masterpass1.out -database postgres -repgrouptype m -nodepriority 1

MASTER1_ID=`java -jar $XDBHOME/bin/edb-repcli.jar -printmdndbid -repsvrfile ~/pubsvrfile.conf | grep -v 'Printing'`

psql -p 5445 -c "select '' || table_schema || '.' || table_name from information_schema.tables where table_schema = 'public' and  table_name like '%table%'" | grep "\." > ~/tables.txt

java -jar $XDBHOME/bin/edb-repcli.jar -createpub my_pub -repsvrfile ~/pubsvrfile.conf -pubdbid $MASTER1_ID -reptype T -tables `cat ~/tables.txt | tr '\n' ' '` -repgrouptype m

java -jar $XDBHOME/bin/edb-repcli.jar -addpubdb -repsvrfile ~/pubsvrfile.conf -dbtype enterprisedb -dbhost localhost -dbport 5446 -dbuser enterprisedb -dbpassfile ~/masterpass2.out -database postgres -repgrouptype m -nodepriority 2 -replicatepubschema false 

java -jar $XDBHOME/bin/edb-repcli.jar -confschedulemmr $MASTER1_ID -pubname my_pub -repsvrfile ~/pubsvrfile.conf -realtime 10

#
# Launch PEM client for manipulating DB and the xDB console for viewing replication.
# When replication console is existed, the container will exit.
#
/opt/PEM/client-v6/scripts/launchPEMClient.sh
$XDBHOME/bin/runRepConsole.sh
