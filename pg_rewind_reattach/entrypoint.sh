#!/bin/bash -l
echo ""
echo "***********************************************************************************"
echo "***********************************************************************************"
echo ""
echo "Running pg_rewind Reattach example entrypoint script"
echo "..."

cd ~/as9.6

echo "***********************************************************************************"
echo ""
echo "1 Set up the databases for the test"
echo ""
echo "... 1.1 Create the initial Primary EPAS 9.6 database cluster via intidb command"
/usr/edb/as9.6/bin/initdb -D /var/lib/edb/as9.6/data_a

echo ""
echo "...... 1.1.2 Set postgresql.conf configuration parameters to support streaming replication using a replication slot"
sed -i 's/#wal_level = minimal/wal_level = replica/' /var/lib/edb/as9.6/data_a/postgresql.conf
sed -i 's/#max_wal_senders = 0/max_wal_senders = 3/' /var/lib/edb/as9.6/data_a/postgresql.conf 
sed -i 's/#max_replication_slots = 0/max_replication_slots = 3/' /var/lib/edb/as9.6/data_a/postgresql.conf
sed -i 's/#hot_standby = off/hot_standby = on/' /var/lib/edb/as9.6/data_a/postgresql.conf
sed -i 's/#wal_keep_segments = 0/wal_keep_segments = 20/' /var/lib/edb/as9.6/data_a/postgresql.conf
sed -i 's/#wal_log_hints = off/wal_log_hints = on/' /var/lib/edb/as9.6/data_a/postgresql.conf

echo ""
echo "......... 1.1.2.1 Show parameters configured in postgresql.conf file"
grep "port =" /var/lib/edb/as9.6/data_a/postgresql.conf 
grep wal_level /var/lib/edb/as9.6/data_a/postgresql.conf
grep max_wal_senders /var/lib/edb/as9.6/data_a/postgresql.conf
grep max_replication_slots /var/lib/edb/as9.6/data_a/postgresql.conf
grep "hot_standby =" /var/lib/edb/as9.6/data_a/postgresql.conf
grep wal_keep_segments /var/lib/edb/as9.6/data_a/postgresql.conf
grep wal_log_hints /var/lib/edb/as9.6/data_a/postgresql.conf
grep full_page_writes /var/lib/edb/as9.6/data_a/postgresql.conf

echo ""
echo "...... 1.1.3 Authorize replication connections in pg_hba.conf"
sed -i 's/#host[ ]*replication/host replication/g' /var/lib/edb/as9.6/data_a/pg_hba.conf

echo ""
echo "......... 1.1.3.1 Run tail to show end of pg_hba.conf file"
tail /var/lib/edb/as9.6/data_a/pg_hba.conf

echo ""
echo "...... 1.1.4 Start the initial primary EPAS v9.6 cluster"
/usr/edb/as9.6/bin/pg_ctl -w -D /var/lib/edb/as9.6/data_a -l /var/lib/edb/as9.6/data_a/startup.log start

echo ""
echo "...... 1.1.5 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "...... 1.1.6 Give enterprisedb database user a password"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "ALTER USER enterprisedb IDENTIFIED BY enterprisedb"

#echo ""
#echo "1.1.7 Create a replication slot ['s2_slot'] in Primary database for standby database"
#/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_create_physical_replication_slot('s2_slot')"
#/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_replication_slots"

echo ""
echo "... 1.2 Create the initial Standby EPAS 9.6 database cluster via a pg_basebackup of primary cluster"
/usr/edb/as9.6/bin/pg_basebackup -D /var/lib/edb/as9.6/data_b -p 5444 -h 127.0.0.1

echo ""
echo "...... 1.2.1 Set port of Standby database to port 5445"
sed -i 's/port = 5444/port = 5445/' /var/lib/edb/as9.6/data_b/postgresql.conf

echo ""
echo "...... 1.2.2 Create recovery.conf file in Standby instance and show contents"
cat <<EOF > /var/lib/edb/as9.6/data_b/recovery.conf
recovery_target_timeline = 'latest'
standby_mode = 'on'
primary_conninfo = 'user=enterprisedb host=127.0.0.1 port=5444 sslmode=prefer sslcompression=1'
#primary_slot_name = 's2_slot'
trigger_file = 's2_trigger.file'
EOF

cat /var/lib/edb/as9.6/data_b/recovery.conf

echo ""
echo "...... 1.2.3 Start the initial secondary EPAS v9.6 cluster"
/usr/edb/as9.6/bin/pg_ctl -w -D /var/lib/edb/as9.6/data_b -l /var/lib/edb/as9.6/data_b/startup.log start

echo ""
echo "...... 1.2.4 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "...... 1.2.5 Check streaming replication status of master [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -x -c "select * from pg_stat_replication;"

echo ""
echo "...... 1.2.6 Check if standby [port 5445] is in recovery"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -x -c "select pg_is_in_recovery();"

echo ""
echo "result should be pg_is_in_recovery = t"


echo ""
echo "1.3 Test the primary database and standby databases"
echo "1.3.1 Test the primary cluster [port 5444]"
echo "1.3.1.1 Create my_table in edb database in primary cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "CREATE TABLE my_table ( my_col text )"

echo ""
echo "1.3.1.2 List tables in edb database in primary cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "\dt"

echo ""
echo "1.3.1.3 Insert into my_table in edb database in primary cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "INSERT INTO my_table VALUES (E'Hello World\!')"

echo ""
echo "1.3.1.4 Query my_table in edb database in primary cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "TABLE my_table"

# may need sleep here
sleep 2

echo ""
echo "1.3.2 Test the standby cluster [port 5445]"
echo "1.3.2.1 List tables in edb database in standby [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "\dt"

echo ""
echo "1.3.2.2 Query my_table in edb database in standby [Port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo ""
echo "1.3.2.3 Attempt to insert into my_table in edb database in standby [Port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "INSERT INTO my_table VALUES (E'Well, hello there\!')"

echo ""
echo "1.3.1.4 Create my_table in edb database in standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "CREATE TABLE my_table2 ( my_col text )"

echo ""
echo "1.3.2.5 List tables in edb database in standby [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "\dt"

echo ""
echo "1.3.2.6 Query my_table in edb database in standby [Port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo "***********************************************************************************"
echo ""
echo ""
echo "2 Make standby cluster [port 5445] a standalone cluster (i.e., take out of replication)"
echo ""
echo "... 2.1 Make the current standby [port 5445] as new primary by running pg_ctl promote or creating a trigger file."

echo ""
echo "...... 2.1.1 show trigger_file setting in recovery.conf file in current standby [port 5445]"
grep trigger_file /var/lib/edb/as9.6/data_b/recovery.conf

echo ""
echo "...... 2.1.2 touch the trigger file"
touch /var/lib/edb/as9.6/data_b/s2_trigger.file

echo "...... 2.1.3 ls showing trigger file"
ls /var/lib/edb/as9.6/data_b/*trigger*

echo ""
echo "* Note: could have used \"pg_ctl -D data_b promote\" instead of a trigger file"

# may need sleep here
sleep 5

echo ""
echo "...... 2.3.4 Check if the old standby is still in recovery"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -x -c "select pg_is_in_recovery();"

echo ""
echo "result should be pg_is_in_recovery = f"

echo ""
echo "...... 2.3.5 Check latest database log if desired to see that Standby has been successfully promoted as a master"
ls -lrt -d /var/lib/edb/as9.6/data_b/pg_log/*.log | tail -n1
cat "$(ls -rt -d /var/lib/edb/as9.6/data_b/pg_log/*.log | tail -n1)"

echo ""
echo "...... 2.3.6 List running 'edb-post' processes"
ps -ef | grep edb-post


echo ""
echo "...... 2.3.7 Check streaming replication status of master [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -x -c "select * from pg_stat_replication;"


echo ""
echo "... 2.7 Test primary and standby databases"

echo ""
echo "...... 2.7.1 Test the original master cluster [port 5444]"
echo "......... 2.7.1.1 List tables in edb database on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "\dt"

echo ""
echo "......... 2.7.1.2 Query my_table on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "TABLE my_table"

echo ""
echo "......... 2.7.1.3 Attempt to insert into my_table on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "INSERT INTO my_table VALUES (E'Howdy\!')"

echo ""
echo "......... 2.7.1.4 Query my_table on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "TABLE my_table"

echo ""
echo "......... 2.7.1.5 Attempt to create a a new table on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "CREATE TABLE my_table2a( my_col2a text )"

echo ""
echo "......... 2.7.1.6 List tables in edb database on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "\dt"

echo ""
echo "......... 2.7.1.7 Insert data into my_table2a test table on original master cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "INSERT INTO my_table2a VALUES (E'You say goodbye. I say hello\!')"

echo ""
echo "......... 2.7.1.8 Query my_table2a on original master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "TABLE my_table2a"

# may need sleep here
sleep 2

echo ""
echo "...... 2.7.2 Test the new standalone cluster [port 5445]"
echo "......... 2.7.2.1 List tables in edb database on old standby database [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "\dt"

echo ""
echo "......... 2.7.2.2 Query my_table on old standby database [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo "......... 2.7.2.3 Insert data into my_table test table on old standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "INSERT INTO my_table VALUES (E'Hola Mundo\!')"

echo ""
echo "......... 2.7.2.4 Query my_table test table on old standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo ""
echo "......... 2.7.2.5 Create a a new table in old standby database [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "CREATE TABLE my_table2b ( my_col2b text )"

echo ""
echo "......... 2.7.2.6 List tables in edb database in old standby database [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "\dt"

echo "......... 2.7.2.7 Insert data into my_table2b test table on old standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "INSERT INTO my_table2b VALUES (E'Goodbye\!')"

echo ""
echo "......... 2.7.2.8 Query my_table2b test table on old standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table2b"


echo "***********************************************************************************"
echo ""
echo "3 Re-attach the old standby cluster [port 5445] to the original master [port 5444]"
echo "... 3.1 Do clean shutdown of the old standby cluster [port 5445] (-m fast or smart)"
/usr/edb/as9.6/bin/pg_ctl -D /var/lib/edb/as9.6/data_b stop -mf

echo ""
echo "...... 3.1.1 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "... 3.2 Use pg_rewind to reattach old standby cluster [port 5445] to the original master [port 5444]"
/usr/edb/as9.6/bin/pg_rewind -D /var/lib/edb/as9.6/data_b --source-server="port=5444 dbname=edb host=127.0.0.1 user=enterprisedb"

echo ""
echo "... 3.3 Prepare reattached database to be restarted as a standby"
echo ""
echo "...... 3.3.1 Let's list the contents of the standby cluster's data directory after the pg_rewind was performed"
ls -l /var/lib/edb/as9.6/data_b

echo ""
echo "...... 3.3.1 Let's look at the contents of the backup_label file"
cat /var/lib/edb/as9.6/data_b/backup_label

echo ""
echo "...... 3.3.2 Set port of Standby database to port 5445"
sed -i 's/port = 5444/port = 5445/' /var/lib/edb/as9.6/data_b/postgresql.conf

echo ""
echo "...... 3.3.3 Remove old recovery.done file"
rm /var/lib/edb/as9.6/data_b/recovery.done

echo ""
echo "...... 3.3.4 Remove old unnecessary files from standby data directory (optional, but good practice)"
rm /var/lib/edb/as9.6/data_b/backup_label*

echo ""
echo "...... 3.3.5 Create a recovery.conf file in Standby instance and show contents"
echo "......... may want to do this from a copy of previous recovery.conf in the standby cluster data directory"
cat <<EOF > /var/lib/edb/as9.6/data_b/recovery.conf
recovery_target_timeline = 'latest'
standby_mode = 'on'
primary_conninfo = 'user=enterprisedb host=127.0.0.1 port=5444 sslmode=prefer sslcompression=1'
#primary_slot_name = 's2_slot'
trigger_file = 's2_trigger.file'
EOF

cat /var/lib/edb/as9.6/data_b/recovery.conf

echo ""
echo "... 3.4 Start the standby EPAS v9.6 cluster and verify status"
echo "...... 3.4.1 Start the standby cluster [port 5445]"
/usr/edb/as9.6/bin/pg_ctl -w -D /var/lib/edb/as9.6/data_b -l /var/lib/edb/as9.6/data_b/startup.log start

echo ""
echo "...... 3.4.2 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "...... 3.4.3 Check streaming replication status of master [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -x -c "select * from pg_stat_replication;"

echo ""
echo "...... 3.4.4 Check if standby [port 5445] is in recovery"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -x -c "select pg_is_in_recovery();"

echo ""
echo "result should be pg_is_in_recovery = t"

echo ""
echo "...... 3.4.5 Check latest database logs if desired to see that Standby [on port 5445] has been successfully restarted"
ls -lrt -d /var/lib/edb/as9.6/data_b/pg_log/*.log | tail -n1
cat "$(ls -rt -d /var/lib/edb/as9.6/data_b/pg_log/*.log | tail -n1)"


echo ""
echo "... 3.5 Test the primary and standby clusters"
echo ""
echo "...... 3.5.1 Test the primary cluster [port 5444]"

echo "......... 3.5.1.1 List tables in edb database on the master database [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "\dt"

echo "......... 3.5.1.2 Insert data into my_table test table on master database cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "INSERT INTO my_table VALUES (E'Bon Jour\!')"

echo ""
echo "......... 3.5.1.3 Query my_table test table on master database cluster [port 5444]"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "TABLE my_table"

# may need sleep here
sleep 2

echo ""
echo "...... 3.5.2 Test the standby cluster [port 5445]"

echo ""
echo "......... 3.5.2.1 List tables in edb database on the standby database [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "\dt"

echo ""
echo "......... 3.5.2.2 Query my_table on standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table"
echo ""
echo "......... 3.5.2.3 Query my_table2a on standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table2a"

echo ""
echo "......... 3.5.2.4 Attempt to insert into my_table on standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "INSERT INTO my_table VALUES (E'Greetings\!')"

echo "......... 3.5.2.5 Query my_table on standby cluster [port 5445]"
/usr/edb/as9.6/bin/psql -p 5445 -d edb -c "TABLE my_table"


echo "***********************************************************************************"
echo "***********************************************************************************"

#Uncomment the following to enter into docker instance for further inspection if desired
#echo ""
echo "Opening bash prompt"
echo "..."
/bin/bash -l

echo ""
echo "Exiting Streaming Replication Switchover-Switchback entrypoint script"
echo ""




