#!/bin/bash
yum -y install which 
yum -y install sudo nc 
echo -e "enterprisedb\nenterprisedb" | (passwd --stdin enterprisedb) 
gpasswd -a enterprisedb wheel 
sed -i 's/%wheel.*/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
sed -i 's/Defaults.*requiretty/Defaults !requiretty/' /etc/sudoers

