#!/bin/bash

# Set environment variables with needed properties
#export YUM_USER=burtwagner
#export YUM_PASSWORD=3b29d54e8cfae1113d1431fee2c3aa0b  
#export MASTER_IP=172.31.19.170
#export STDBY_IP=172.31.19.168
#export WITNESS_IP=35.162.196.248
#export YOUR_EMAIL=burt.wagner@enterprisedb.com
#export PWD_ENCRYPT=2ae8d8d6fbfb742863990dc444e9434f  #see notes above
#export PGDATA=/var/lib/ppas/9.5/master

# Setup YUM repository for installing EPAS and EFM
rpm -Uvh http://yum.enterprisedb.com/edbrepos/edb-repo-9.6-3.noarch.rpm

# Set YUM username/password in edb.repo
sed -i "s/<username>:<password>/$EDB_YUM_USERNAME:$EDB_YUM_PASSWORD/g" /etc/yum.repos.d/edb.repo

# Enable EPAS 9.5 and EFM repo in edb.repo
sed -i "\/ppas95/,/gpgcheck/ s/enabled=0/enabled=1/" /etc/yum.repos.d/edb.repo
sed -i "\/enterprisedb-tools/,/gpgcheck/ s/enabled=0/enabled=1/" /etc/yum.repos.d/edb.repo

# Install EPAS 9.5, EFM, and java
yum -y install ppas95-server efm20 java

