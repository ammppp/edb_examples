#!/bin/bash

/usr/ppas-9.5/bin/pg_ctl -wD /var/lib/ppas/9.5/data start

sudo /bin/java -cp /usr/efm-2.0/lib/EFM-2.0.4.jar com.enterprisedb.hal.main.ServiceCommand start /etc/efm-2.0/efm.properties

/usr/efm-2.0/bin/efm cluster-status efm
exit 0
