#!/bin/bash

# Copy template properties file to properties file, create node list file
cp /etc/efm-2.0/efm.properties.in /etc/efm-2.0/efm.properties
echo `hostname -i`:7800 > /etc/efm-2.0/efm.nodes
chmod 600 /etc/efm-2.0/efm.nodes

# Numerous edits have to be made to the efm.properties file.  These edits include:
# - Username, encrypted password, port, db name for the local database
# - Set port for administrative commands to typically-used 7809
# - Leave local period timeout settings and remote timeout at the default
# - Set user.email to your email address
# - Set bind address
# - Set is witness to false
# - OS username owning the data directory
# - Where the recovery.conf file is located
# - Location of the database binaries
# - VIP will not be used for this exercise
# - Leave the ping server set to Google DNS
# - No fence scripts will be used for this exercise
sed -i "s/db.user=/db.user=enterprisedb/" /etc/efm-2.0/efm.properties
sed -i "s/db.password.encrypted=/db.password.encrypted=2ae8d8d6fbfb742863990dc444e9434f/" /etc/efm-2.0/efm.properties
sed -i "s/db.port=/db.port=5444/" /etc/efm-2.0/efm.properties
sed -i "s/db.database=/db.database=edb/" /etc/efm-2.0/efm.properties
sed -i "s/admin.port=/admin.port=7809/" /etc/efm-2.0/efm.properties
sed -i "s/user.email=/user.email=root@localhost.com/" /etc/efm-2.0/efm.properties
sed -i "s/bind.address=/bind.address=`hostname -i`:7800/" /etc/efm-2.0/efm.properties
sed -i "s/is.witness=/is.witness=false/" /etc/efm-2.0/efm.properties
sed -i "s/db.service.owner=/db.service.owner=enterprisedb/" /etc/efm-2.0/efm.properties
sed -i "s;db.recovery.conf.dir=;db.recovery.conf.dir=/var/lib/ppas/9.5/data;g" /etc/efm-2.0/efm.properties
sed -i "s/db.bin=/db.bin=\/usr\/ppas-9.5\/bin/" /etc/efm-2.0/efm.properties

#/bin/java -cp /usr/efm-2.0/lib/EFM-2.0.4.jar com.enterprisedb.hal.main.ServiceCommand start /etc/efm-2.0/efm.properties
