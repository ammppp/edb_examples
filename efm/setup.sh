/usr/ppas-9.5/bin/initdb -D /var/lib/ppas/9.5/data

#######################################################################
# Configure the necessary settings to permit the clusters to stream logs to the Replication Server
#######################################################################
sed -i 's/#wal_level = minimal/wal_level = logical/' /var/lib/ppas/9.5/data/postgresql.conf 
sed -i 's/#max_wal_senders = 0/max_wal_senders = 5/' /var/lib/ppas/9.5/data/postgresql.conf 
sed -i 's/#max_replication_slots = 0/max_replication_slots = 5/' /var/lib/ppas/9.5/data/postgresql.conf 
sed -i 's/#track_commit_timestamp = off/track_commit_timestamp = on/' /var/lib/ppas/9.5/data/postgresql.conf 

echo "host all all 0.0.0.0/0 trust" >> /var/lib/ppas/9.5/data/pg_hba.conf
sed -i 's/#host[ ]*replication/host replication/g' /var/lib/ppas/9.5/data/pg_hba.conf






