# EDB Example using PEM 7 with EPAS 9.6 

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/containers
edit docker-compose.yml to set valid <efm-key>
mkdir /tmp/edbvolume
docker login -u <EDB Container Username> -p <EDB Container Password> containers.enterprisedb.com

Window 1:
docker-compose up master
 
Window 2:
docker-compose up replica1
 
Window 3:
docker-compose up replica2
 
Window 4:
docker-compose up replica3
 
Window 5 (some commands to run to demo the running containers):
docker-compose exec master sudo -u efm /usr/efm-2.1/bin/efm cluster-status edb
docker-compose exec replica1 sudo -u efm /usr/efm-2.1/bin/efm cluster-status edb
docker-compose exec replica2 sudo -u efm /usr/efm-2.1/bin/efm cluster-status edb
docker-compose exec replica3 sudo -u efm /usr/efm-2.1/bin/efm cluster-status edb

docker-compose exec master psql -d edb -p 5444
docker-compose exec replica1 psql -d edb -p 5444
docker-compose exec replica2 psql -d edb -p 5444
docker-compose exec replica3 psql -d edb -p 5444

docker-compose exec master psql -d edb -p 9999
docker-compose exec replica1 psql -d edb -p 9999
docker-compose exec replica2 psql -d edb -p 9999
docker-compose exec replica3 psql -d edb -p 9999

docker-compose down master

<wait quite a while and keep checking EFM status on the replicas>

<to cleanup, run these commands>
docker-compose down
rm -rf /tmp/edbvolume/*
```
