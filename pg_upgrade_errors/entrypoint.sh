#!/bin/bash -l
echo ""
echo "Running DBMS_SCHEDULER entrypoint script"
echo "..."

echo ""
echo "Create an EDB Postgres Advanced Server v9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/initdb -D /var/lib/ppas/9.5/data

echo ""
echo "Start the EDB Postgres Advanced Server v9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/pg_ctl -w -D /var/lib/ppas/9.5/data -l /var/lib/ppas/9.5/data/startup.log start

echo ""
echo "Create a new database in the 9.5 cluster to be used with DBMS_JOB_SCHEDULER extension"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "CREATE DATABASE testdb;"

echo ""
echo "Install the components required for running jobs using DBMS_SCHECULER package in the testdb database"
echo ".. Create the pgagent schema (catalog) used by the DBMS_SCHEDULER package"
echo ""
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -f /usr/share/ppas95-pgagent-3.4.1/pgagent.sql

#echo ""
#echo ".... List schemas in edb database"
#echo ""
#/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "\dn"

#echo ""
#echo ".... List pgagent schema tables"
#echo ""
#/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "\dt pgagent.*"

echo ".. Create the DBMS_SCHEDULER extension in the edb database"
echo ""
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -c "CREATE EXTENSION dbms_scheduler;"

#
#  The following command in this script will start the pgAgent process that is used to run DBMS_SCHEDULER jobs 
#  by directly calling the pgagent executable.  In a production environment, it is more likely that the
#  process would be started via a service.  The steps and commands for doing so will depend on the version
#  of Linux being used.  For example, under RHEL 7.x/CentOS 7.x/Fedora 7.x, the "systemctl" command would 
#  be used to manage a service for a pgagent process.  When pgAgent is installed with EDB Postgres 
#  Advanced Server (EPAS) via RPM, the following default pgagent service file is installed:
#
#       /usr/lib/systemd/system/ppas95-pgagent.service
#
#  This service file can be used to "install" a pgAgent service configured for the "enterprisedb" 
#  database user to connect to the pgAgent schema installed in the "edb" database in an EPAS cluster 
#  running on port 5444 on the local machine.  As an example, the following commands could be run to
#  start and stop the ppas95-pgagent service, respectively:
#  
#       systemctl start ppas95-pgagent
#      
#       systemctl stop ppas95-pgagent
#
#  If a service were required to run the process connecting to a different database or one on a different 
#  port or as a different database user, it is recommended that a new service file be created from a copy
#  of the ppas95-pgagent.service file.
#
echo ""
echo ".. Start a pgAgent daemon to run DBMS_SCHEUDLER jobs on schedule"
echo ""
/usr/ppas-9.5/bin/pgagent -l 1 -f -s /var/log/ppas-agent-9.5/ppas-pgagent.log hostaddr=localhost port=5444 dbname=testdb user=enterprisedb &

echo ""
echo ".... List the running \"agent\" processes"
echo ""
ps -ef | grep agent

echo ""
echo "Create a job_test table in the edb database for testing job scheduler"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -c "CREATE TABLE job_test ( create_dt date, label varchar2(200) );"

echo ""
echo "Test inserting a record into the job_test table"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -c "INSERT INTO job_test values ( sysdate, 'Non-Job Created Entry @ '||sysdate );"

echo ""
echo "Query the job_test table"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -c "SELECT * FROM job_test ORDER BY 1;"

echo ""
echo "Create a job using the DBMS_SCHEDULER.CREATE_JOB procedure to insert a row into the job_test table every minute"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -f /create_job-insert_test_job.sql


echo ""
echo "Verify that the job has been created and is scheduled to run by querying the dba_scheduler_jobs view"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -f /query-expanded-dba_scheduler_jobs.sql

echo ""
echo "Monitor the job by querying the DBA_SHEDULER_JOBS view and JOB_TEST table every 10 seconds for next 1-1/2 minutes "
echo "to verify job is working and to build up data in the job related tables."
echo "..."
i=0
t=10
z=90
until [ $i -gt $z ]
do
  echo "Check $[(i/t)+1] of $[(z/t)+1]"
  echo "Current date/time: `date`"
  /usr/ppas-9.5/bin/psql -p 5444 -d testdb -f /query-last_job_run-details.sql
  cat /var/log/ppas-agent-9.5/ppas-pgagent.log
  echo ""
  i=$[ $i + $t ]
  
  if [ "$i" -le "$z" ]
  then
    sleep $t
  fi
done

echo "Create an EDB Postgres Advanced Server v9.6 cluster to serve as target of pg_upgrade"
echo "..."
/usr/edb/as9.6/bin/initdb -D /var/lib/edb/as9.6/data

echo ""
echo "Set port of 9.6 cluster to 5445"
echo "..."
sed -i 's/port = 5444/port = 5445/' /var/lib/edb/as9.6/data/postgresql.conf


echo ""
echo "Prepare instances for pg_upgrade"
echo ".. Show that both servers are in trust mode"
echo ".... last 15 lines from v9.5 cluster pg_hba.conf"
echo "..."
tail -n 15 /var/lib/ppas/9.5/data/pg_hba.conf

echo ""
echo ".... last 15 lines from v9.6 cluster pg_hba.conf"
echo "..."
tail -n 15 /var/lib/edb/as9.6/data/pg_hba.conf

echo ""
echo ".. Stop the EDB Postgres Advanced Server v9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/pg_ctl -D /var/lib/ppas/9.5/data stop

echo ""
echo ".. List running processes and see that the database clusters are not running and "
echo "   the previously created pgagent process is not running."
echo "..."
ps -ef

echo ""
echo ".. Run whoami command to show that current user is enterprisedb"
whoami

echo ""
echo ".. Create a directory from where pg_upgrade will be run and navigate to it"
echo "..."
mkdir /var/lib/edb/as9.6/upgrade
cd /var/lib/edb/as9.6/upgrade
pwd

echo ""
echo ".. Perform a upgrade consistency check"
echo "..."
/usr/edb/as9.6/bin/pg_upgrade -d /var/lib/ppas/9.5/data -D /var/lib/edb/as9.6/data -U enterprisedb -b /usr/ppas-9.5/bin -B /usr/edb/as9.6/bin -p 5444 -P 5445 --check -v

echo ""
echo "Perform pg_upgrade"
echo "..."
echo "..."
/usr/edb/as9.6/bin/pg_upgrade -d /var/lib/ppas/9.5/data -D /var/lib/edb/as9.6/data -U enterprisedb -b /usr/ppas-9.5/bin -B /usr/edb/as9.6/bin -p 5444 -P 5445 -v -r

echo ""
echo "Start the EDB Postgres Advanced Server v9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/pg_ctl -w -D /var/lib/ppas/9.5/data -l /var/lib/ppas/9.5/data/startup_postupgrade.log start

echo ""
echo "Start the EDB Postgres Advanced Server v9.6 cluster"
echo "..."
/usr/edb/as9.6/bin/pg_ctl -w -D /var/lib/edb/as9.6/data -l /var/lib/edb/as9.6/data/startup_postupgrade.log start

echo ""
echo "View the contents of the dba_scheduler_jobs view in the v9.5 cluster"
echo ".. Note that a job record exists in the v9.5 testdb database"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -f /query-expanded-dba_scheduler_jobs.sql

echo ""
echo "View the contents of the dba_scheduler_jobs view in the v9.6 cluster"
echo ".. Note that a job record does not exists in the v9.5 testdb database"
echo "..."
/usr/edb/as9.6/bin/psql -p 5445 -d testdb -f /query-expanded-dba_scheduler_jobs.sql

echo ""
echo "View list of extension in the v9.5 testdb database"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -c "\dx"

echo ""
echo "View list of extension in the v9.6 testdb database"
echo ".. Note that the extensions in the 9.6 database is the same as in the 9.5 database"
echo "..."
/usr/edb/as9.6/bin/psql -p 5445 -d testdb -c "\dx"

echo ""
echo "View list of sys.*jobs views in the v9.5 testdb database"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d testdb -c "\dv sys.*jobs"

echo ""
echo "View list of sys.*jobs views in the v9.6 testdb database"
echo ".. Note that all_jobs, dba_jobs, and user_jobs views are not found in the 9.6 database"
echo "..."
/usr/edb/as9.6/bin/psql -p 5445 -d testdb -c "\dv sys.*jobs"


#Uncomment the following to enter into docker instance for further inspection if desired
#echo ""
#echo "Opening bash prompt"
#echo "..."
#/bin/bash -l

echo ""
echo "Exiting DBMS_SCHEDULER entrypoint script"
echo ""




