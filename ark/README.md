# EDB Ark Deployment Quickstart

This Docker container provide a simple way to automate the deployment and configuration of EDB Ark in AWS.

## Prerequisites

- Create an AWS Account and get an Access Key and Access Secret for your account
- In your AWS Account, click the "Accept Software Terms" button for the EDB Ark Console [here](https://aws.amazon.com/marketplace/fulfillment?productId=c109cd0c-93fc-47ba-b079-5e4112608523) if you have not already  
- Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/ark

# Optionally, set environment variables if you don't want to be prompted for them:
# 
# export AWS_ACCESS_KEY_ID=
# export AWS_SECRET_ACCESS_KEY=
# export ARK_EMAIL=
# export ARK_CONSOLE_AMI=
# export ARK_REGION=
# export ARK_KEY_PAIR_NAME=

docker-compose build ark
docker-compose run ark

# Follow the instructions that are printed out
```

## Links of Interest

Make sure that you accept the terms and conditions of these marketplace AMIs before trying to use them:

* [EDB Ark AMIs (click Manaual Launch tab)](https://aws.amazon.com/marketplace/fulfillment?productId=c109cd0c-93fc-47ba-b079-5e4112608523) 
* RHEL AMIs for cluster servers (default user is ec2-user):
    * [RHEL 6 AMIs (click Manual Launch tab)](https://aws.amazon.com/marketplace/fulfillment?productId=04260433-f504-44ee-8c3e-e0cd377e8a3c)
    * [RHEL 7 AMIs (click Manual Launch tab)](https://aws.amazon.com/marketplace/fulfillment?productId=1dbbb9c1-b882-4575-947e-23660fc5991b)
* CentOS AMIs for cluster servers (default usr is centos):
    * [CentOS 6 AMIs (click Manual Launch tab)](https://aws.amazon.com/marketplace/fulfillment?productId=74e73035-3435-48d6-88e0-89cc02ad83ee)
    * [CentOS 7 AMIs (click Manual Launch tab)](https://aws.amazon.com/marketplace/fulfillment?productId=b7ee8a69-ee97-4a49-9e68-afaee216db2e)