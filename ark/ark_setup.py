import os

#########################################################################
##
## Variables to set, if not set in env, prompt user for the values 
##
#########################################################################
try:  
  os.environ["AWS_ACCESS_KEY_ID"]
except KeyError: 
  os.environ["AWS_ACCESS_KEY_ID"] = raw_input("\nEnter your AWS_ACCESS_KEY_ID: ")

try:
  os.environ["AWS_SECRET_ACCESS_KEY"]
except KeyError:
  os.environ["AWS_SECRET_ACCESS_KEY"] = raw_input("\nEnter your AWS_SECRET_ACCESS_KEY: ")

try:
  os.environ["ARK_EMAIL"]
except KeyError:
  os.environ["ARK_EMAIL"] = raw_input("\nEnter your default ARK_EMAIL: ")

try:
  os.environ["ARK_CONSOLE_AMI"]
except KeyError:
  print('\nAvailable Ark Console AMIs are listed on the Manual Launch tab here:  https://aws.amazon.com/marketplace/fulfillment?productId=c109cd0c-93fc-47ba-b079-5e4112608523')
  os.environ["ARK_CONSOLE_AMI"] = raw_input("\nEnter your ARK_CONSOLE_AMI: ")

try:
  os.environ["ARK_REGION"]
except KeyError:
  os.environ["ARK_REGION"] = raw_input("\nEnter your ARK_REGION: ")

try:
  os.environ["ARK_KEY_PAIR_NAME"]
except KeyError:
  os.environ["ARK_KEY_PAIR_NAME"] = raw_input("\nEnter your ARK_KEY_PAIR_NAME (can be empty): ")

try:
  os.environ["ARK_SUBNET_ID"]
except KeyError:
  os.environ["ARK_SUBNET_ID"] = raw_input("\nEnter your ARK_SUBNET_ID (can be empty): ")


#########################################################################
##
## Optional variables to set 
##
#########################################################################
ARK_TIMEZONE='EST' #Only change if you know what options are available
ARK_SERVICE_USER='ark-service-user'
ARK_SERVICE_ROLE='ark-service-role'
ARK_USER_ROLE='ark-user-role'
ARK_SECURITY_GROUP='ark-security-group'


#########################################################################
##
## Main driver  
##
#########################################################################
def main():

    accountId=boto3.resource('iam').CurrentUser().arn.split(':')[4]

    iamClient = boto3.client('iam')
    ec2Client = boto3.client('ec2', os.environ["ARK_REGION"])

    setupArkServiceUser(iamClient)
    setupArkServiceRole(iamClient, accountId)
    setupArkUserRole(iamClient, accountId)
    setupSecurityGroup(ec2Client)
    launchConsole(iamClient, ec2Client, accountId)

#########################################################################
##
## Adding the ARK_SERVICE_USER
##
#########################################################################
def setupArkServiceUser(iamClient):

    print('\nChecking if {} user already exists...'.format(ARK_SERVICE_USER))
    for user in iamClient.list_users(PathPrefix='/',MaxItems=100)['Users']:
   
        if user['UserName'] == ARK_SERVICE_USER:

            print('   Found user to delete')

            print('   Checking for existing access keys...')
            response = iamClient.list_access_keys(
                UserName=ARK_SERVICE_USER,
                MaxItems=123
            )
            for accessKey in response['AccessKeyMetadata']:
                print('   Deleting access key {}...'.format(accessKey['AccessKeyId']))
                iamClient.delete_access_key(
                    UserName=ARK_SERVICE_USER,
                    AccessKeyId=accessKey['AccessKeyId']
                )

            print('   Checking for existing user policies...')
            response = iamClient.list_user_policies(
                UserName=ARK_SERVICE_USER,
                MaxItems=123
            )
            for accessKey in response['PolicyNames']:
                print('   Deletinge user policy: {}'.format(accessKey))
                iamClient.delete_user_policy(
                    UserName=ARK_SERVICE_USER,
                    PolicyName='ark-service-user-policy'
                )

            print('   Deleting user')
            iamClient.delete_user(UserName=ARK_SERVICE_USER)

    print('\nCreating {} user...'.format(ARK_SERVICE_USER))
    iamClient.create_user( Path='/', UserName=ARK_SERVICE_USER)

    print('   Adding inline {}  policy...'.format(ARK_SERVICE_USER))
    response = iamClient.put_user_policy(
        UserName=ARK_SERVICE_USER,
        PolicyName='ark-service-user-policy',
        PolicyDocument= textwrap.dedent("""\
            {
                "Version": "2012-10-17",
                "Statement": [
                {
                    "Sid": "Stmt1389628412000",
                    "Effect": "Allow",
                    "Action": [
                        "sts:GetFederationToken",
                        "sts:AssumeRole"
                    ],
                    "Resource": ["*"]
                }]
            }""")
    )


#########################################################################
##
## Adding the ARK_SERVICE_ROLE
##
#########################################################################
def setupArkServiceRole(iamClient, accountId):

    print('\nChecking if {} role already exists...'.format(ARK_SERVICE_ROLE))
    for role in iamClient.list_roles(PathPrefix='/')['Roles']:
        if role['RoleName'] == ARK_SERVICE_ROLE:

            print('   Checking for existing role policies...')
            response = iamClient.list_role_policies(RoleName=ARK_SERVICE_ROLE)
            for policyName in response['PolicyNames']:
                print('   Deletinge role policy: {}'.format(policyName))
                iamClient.delete_role_policy(
                    RoleName=ARK_SERVICE_ROLE,
                    PolicyName=policyName
                )

            print('   Deleting role {}...'.format(role['RoleName']))
            iamClient.delete_role(RoleName=role['RoleName'])


    print('\nCreate {} role...'.format(ARK_SERVICE_ROLE))
    response = iamClient.create_role(
        Path='/',
        RoleName=ARK_SERVICE_ROLE,
        AssumeRolePolicyDocument=textwrap.dedent("""\
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "",
                        "Effect": "Allow",
                        "Principal": {
                             "Service": "ec2.amazonaws.com"
                        },
                        "Action": "sts:AssumeRole"
                    },
                    {
                        "Sid": "",
                        "Effect": "Allow",
                        "Principal": {
                            "AWS": "arn:aws:iam::"""+accountId+""":root"
                        },
                        "Action": "sts:AssumeRole",
                        "Condition": {
                             "StringEquals": {
                                 "sts:ExternalId": "EDB-ARK-SERVICE"
                             }
                        }
                    }
                ]
            }""")
        ) 

    print('   Add inline policy to role...')
    response = iamClient.put_role_policy(
        RoleName=ARK_SERVICE_ROLE,
        PolicyName='ark-service-role-policy',
        PolicyDocument="""{ 
            "Version": "2012-10-17", 
            "Statement": [ 
                { 
                    "Effect": "Allow", 
                    "Action": "s3:*", 
                    "Resource": "arn:aws:s3:::*" 
                } 
            ] 
        }"""
    )

#########################################################################
##
## Adding the ARK_USER_ROLE
##
#########################################################################
def setupArkUserRole(iamClient, accountId):

    print('\nChecking if {} role already exists...'.format(ARK_USER_ROLE))
    for role in iamClient.list_roles(PathPrefix='/')['Roles']:
        if role['RoleName'] == ARK_USER_ROLE:
   
            print('   Checking for existing role policies...')
            response = iamClient.list_role_policies(RoleName=ARK_USER_ROLE)
            for policyName in response['PolicyNames']:
                print('   Deletinge role policy: {}'.format(policyName))
                iamClient.delete_role_policy(
                    RoleName=ARK_USER_ROLE,
                    PolicyName=policyName
                )
            try:
                iamClient.remove_role_from_instance_profile(
                    InstanceProfileName=ARK_USER_ROLE,
                    RoleName=ARK_USER_ROLE
                )
            except Exception as e:
                print('   Did not remove role from instance profile: {}'.format(
                    e.response['Error']['Code']))   

            try:
                response = iamClient.delete_instance_profile(
                    InstanceProfileName=ARK_USER_ROLE
                )
            except Exception as e:
                print('   Did not remove instance profile: {}'.format(
                    e.response['Error']['Code']))    

            print('   Deleting role {}...'.format(role['RoleName']))
            iamClient.delete_role(RoleName=role['RoleName'])


    print('\nCreate {} role...'.format(ARK_USER_ROLE))
    response = iamClient.create_role(
        Path='/',
        RoleName=ARK_USER_ROLE,
        AssumeRolePolicyDocument=textwrap.dedent("""\
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "",
                        "Effect": "Allow",
                        "Principal": {
                            "Service": "ec2.amazonaws.com"
                        },
                        "Action": "sts:AssumeRole"
                    },
                    {
                        "Sid": "",
                        "Effect": "Allow",
                        "Principal": {
                            "AWS": [
                                "arn:aws:iam::"""+accountId+""":root"
                            ]
                        },
                    "Action": "sts:AssumeRole",
                    "Condition": {
                        "StringLike": {
                            "sts:ExternalId": "*"
                        }
                    }
                    }
                ]
            }""")
    )

    print('   Add inline policy to role...')
    response = iamClient.put_role_policy(
        RoleName=ARK_USER_ROLE,
        PolicyName='ark-user-role-policy',
        PolicyDocument=textwrap.dedent("""\
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Action": [
                            "ec2:AllocateAddress",
                            "ec2:AssignPrivateIpAddresses",
                            "ec2:Associate*",
                            "ec2:Attach*",
                            "ec2:AuthorizeSecurityGroup*",
                            "ec2:Copy*",
                            "ec2:Create*",
                            "ec2:DeleteInternetGateway",
                            "ec2:DeleteNetworkAcl",
                            "ec2:DeleteNetworkAclEntry",
                            "ec2:DeleteNetworkInterface",
                            "ec2:DeletePlacementGroup",
                            "ec2:DeleteRoute",
                            "ec2:DeleteRouteTable",
                            "ec2:DeleteSecurityGroup",
                            "ec2:DeleteSnapshot",
                            "ec2:DeleteSubnet",
                            "ec2:DeleteTags",
                            "ec2:DeleteVolume",
                            "ec2:DeleteVpc",
                            "ec2:DeleteKeypair",
                            "ec2:Describe*",
                            "ec2:Detach*",
                            "ec2:DisassociateAddress",
                            "ec2:DisassociateRouteTable",
                            "ec2:EnableVolumeIO",
                            "ec2:GetConsoleOutput",
                            "ec2:ModifyImageAttribute",
                            "ec2:ModifyInstanceAttribute",
                            "ec2:ModifyNetworkInterfaceAttribute",
                            "ec2:ModifySnapshotAttribute",
                            "ec2:ModifyVolumeAttribute",
                            "ec2:ModifyVpcAttribute",
                            "ec2:MonitorInstances",
                            "ec2:ReleaseAddress",
                            "ec2:ReplaceNetworkAclAssociation",
                            "ec2:ReplaceNetworkAclEntry",
                            "ec2:ReplaceRoute",
                            "ec2:ReplaceRouteTableAssociation",
                            "ec2:ReportInstanceStatus",
                            "ec2:ResetImageAttribute",
                            "ec2:ResetInstanceAttribute",
                            "ec2:ResetNetworkInterfaceAttribute",
                            "ec2:ResetSnapshotAttribute",
                            "ec2:RevokeSecurityGroup*",
                            "ec2:RunInstances",
                            "ec2:StartInstances",
                            "ec2:UnassignPrivateIpAddresses",
                            "ec2:UnmonitorInstances"
                        ],
                        "Resource": "*",
                        "Effect": "Allow",
                        "Sid": "Stmt1407961327680"
                    },
                    {
                        "Action": [
                            "iam:PassRole"
                        ],
                        "Resource": "*",
                        "Effect": "Allow",
                        "Sid": "Stmt1407961362664"
                    },
                    {
                        "Action": [
                            "s3:CreateBucket",
                            "s3:Get*",
                            "s3:List*"
                        ],
                        "Resource": "*",
                        "Effect": "Allow",
                        "Sid": "Stmt1407961630932"
                    },
                    {
                        "Action": [
                            "s3:Put*",
                            "s3:Get*",
                            "s3:DeleteObject*"
                        ],
                        "Resource": "arn:aws:s3:::*/wal_005*",
                        "Effect": "Allow",
                        "Sid": "Stmt1407961734627"
                    },
                    {
                        "Condition": {
                            "StringEquals": {
                                "ec2:ResourceTag/CreatedBy": "EnterpriseDB"
                            }
                        },
                        "Action": [
                            "ec2:RebootInstances",
                            "ec2:StopInstances",
                            "ec2:TerminateInstances"
                        ],
                        "Resource": "*",
                        "Effect": "Allow",
                        "Sid": "Stmt1407961927870"
                    }
                ]
            }""")
    )

    # The AWS GUI automatically create and attaches an instance profile
    # to an EC2 Role, so via API we have to do that manually
    iamClient.create_instance_profile(
        InstanceProfileName=ARK_USER_ROLE,
        Path='/'
    )
    iamClient.add_role_to_instance_profile(
        InstanceProfileName=ARK_USER_ROLE,
        RoleName=ARK_USER_ROLE
    )



#########################################################################
##
## Create or update the security group for console 
##
#########################################################################
def setupSecurityGroup(ec2Client):

    print('\nCreating or updating security group')

    # Try to get the security group
    response = ec2Client.describe_security_groups(
        Filters=[
            {
                'Name': 'group-name',
                'Values': [
                    ARK_SECURITY_GROUP,
                ]
            },
        ]
    )

    # If the security grop does not exist, create it
    if len(response['SecurityGroups']) == 0:
        print('   Creating security group...')
        ec2Client.create_security_group(
            GroupName=ARK_SECURITY_GROUP,
            Description='Ark Security Group for Ark Console'
        )

    # If the security group has security settings, remove them
    response = ec2Client.describe_security_groups(
        GroupNames=[ARK_SECURITY_GROUP]
    )
    ARK_SECURITY_GROUP_ID=response['SecurityGroups'][0]['GroupId']

    if len(response['SecurityGroups'][0]['IpPermissions']) > 0:
         print('   Removing existing authorizations from security group')
         ec2Client.revoke_security_group_ingress(
             GroupName=ARK_SECURITY_GROUP,
             IpPermissions=response['SecurityGroups'][0]['IpPermissions']
         )

    # Add the necessary security settings to the group
    print('   Adding authorizations to security group')  
    ec2Client.authorize_security_group_ingress(
        GroupName=ARK_SECURITY_GROUP,
        IpPermissions=[
            {
                'IpProtocol': 'tcp',
                'FromPort': 80,
                'ToPort': 80,
                'IpRanges': [ { 'CidrIp': '0.0.0.0/0' } ],
            },
            {   
                'IpProtocol': 'tcp',
                'FromPort': 443,
                'ToPort': 443,
                'IpRanges': [ { 'CidrIp': '0.0.0.0/0' } ],
            },
            {   
                'IpProtocol': 'tcp',
                'FromPort': 7800,
                'ToPort': 7900,
                'IpRanges': [ { 'CidrIp': '0.0.0.0/0' } ],
            },
            {   
                'IpProtocol': 'tcp',
                'FromPort': 6666,
                'ToPort': 6666,
                'IpRanges': [ { 'CidrIp': '0.0.0.0/0' } ],
            },
            {   
                'IpProtocol': 'tcp',
                'FromPort': 22,
                'ToPort': 22,
                'IpRanges': [ { 'CidrIp': '0.0.0.0/0' } ],
            },
            {   
                'IpProtocol': 'icmp',
                'FromPort': -1,
                'ToPort': -1,
                'IpRanges': [ { 'CidrIp': '0.0.0.0/0' } ],
            },

        ]
    )


#########################################################################
##
## Launch the Ark console 
##
#########################################################################
def launchConsole(iamClient, ec2Client, accountId):

    print('\nLaunching Ark Console Instance...')

    arkServiceRoleArn = iamClient.get_role(RoleName=ARK_SERVICE_ROLE)['Role']['Arn']
    arkUserRoleArn = iamClient.get_role(RoleName=ARK_USER_ROLE)['Role']['Arn']

    print('   Creating access key...')
    response = iamClient.create_access_key(UserName=ARK_SERVICE_USER)
    arkServiceUserAccessKeyId = response['AccessKey']['AccessKeyId']
    arkServiceUserSecretAccessKey = response['AccessKey']['SecretAccessKey']

    USER_DATA=textwrap.dedent("""\
        #!/bin/bash -x

        echo 'setting local time to EST'
        rm -f /etc/localtime
        ln -s /usr/share/zoneinfo/""" + ARK_TIMEZONE + """ /etc/localtime
        rm -f /etc/timezone
        ln -s /usr/share/zoneinfo/""" + ARK_TIMEZONE + """ /etc/timezone

        echo 'restarting ark services'
        /etc/init.d/postgresql-9.6 restart
        su - ppcd -c "asadmin restart-domain"

        echo 'creating directory for backups'
        mkdir /var/ppcd/backups
        chown ppcd:ppcd /var/ppcd/backups

        echo 'writing properties file'
        mv ~ppcd/ppcd.properties ~ppcd/ppcd.properties.original
        cat <<EOF > ~ppcd/ppcd.properties
        console.db.user=postgres
        console.db.password=0f42d1934a1a19f3d25d6288f2a3272c6143fc5d
        console.db.name=postgres
        console.db.backup.script=/var/ppcd/.edb/backup-postgresql.sh
        console.db.backup.dir=/var/ppcd/backups
        # console.db.backup.container=
        # console.db.backup.folder=
        contact.email.address=""" + os.environ["ARK_EMAIL"] + """
        email.from.address=""" + os.environ["ARK_EMAIL"] + """
        notification.email=""" + os.environ["ARK_EMAIL"] + """
        wal.archive.container=ark-wal-bucket-""" + accountId  + """
        api.timeout=10
        service.account.id=edbArk_service_user
        service.account.password=password
        aws.service.account.rolearn=""" + arkServiceRoleArn + """
        aws.service.account.externalid=EDB-ARK-SERVICE
        aws.region=""" + os.environ["ARK_REGION"] + """
        aws.cross.account.accesskey=""" + arkServiceUserAccessKeyId + """
        aws.cross.account.secretkey=""" + arkServiceUserSecretAccessKey + """
        self.registration.enabled=true
        console.dashboard.docs=DEFAULT
        console.dashboard.hot.topics=DEFAULT
        EOF

        chown ppcd:ppcd ~ppcd/ppcd.*
        chmod 700 ~ppcd/ppcd.*

        echo 'running postInstall script'
        echo -e "y\\n" | /var/ppcd/postInstall.sh""")

    if os.environ["ARK_KEY_PAIR_NAME"] == '':
        if os.environ["ARK_SUBNET_ID"] == '':
            response = ec2Client.run_instances(ImageId=os.environ["ARK_CONSOLE_AMI"], MinCount=1,
                MaxCount=1, SecurityGroups=[ARK_SECURITY_GROUP],
                UserData=USER_DATA, InstanceType='m4.large')
        else :
            response = ec2Client.run_instances(ImageId=os.environ["ARK_CONSOLE_AMI"], MinCount=1,
                MaxCount=1, SecurityGroupIds=['sg-07eae190'],
                UserData=USER_DATA, InstanceType='m4.large', SubnetId=os.environ["ARK_SUBNET_ID"])
      
    else :
        if os.environ["ARK_SUBNET_ID"] == '':
            response = ec2Client.run_instances( ImageId=os.environ["ARK_CONSOLE_AMI"], MinCount=1,
                MaxCount=1, KeyName=os.environ["ARK_KEY_PAIR_NAME"], SecurityGroups=[ARK_SECURITY_GROUP],
                UserData=USER_DATA, InstanceType='m4.large')
        else :
            response = ec2Client.run_instances( ImageId=os.environ["ARK_CONSOLE_AMI"], MinCount=1,
                MaxCount=1, KeyName=os.environ["ARK_KEY_PAIR_NAME"], SecurityGroupIds=['sg-07eae190'],
                UserData=USER_DATA, InstanceType='m4.large', SubnetId=os.environ["ARK_SUBNET_ID"])

    ARK_CONSOLE_INSTANCE=response['Instances'][0]['InstanceId']
    print('   Starting instance {}, please wait...'.format(ARK_CONSOLE_INSTANCE))
    ARK_STATUS=''

    while ARK_STATUS != 'running': 
        time.sleep(5)
        result = ec2Client.describe_instances(InstanceIds=[ARK_CONSOLE_INSTANCE])
        ARK_STATUS=result['Reservations'][0]['Instances'][0]['State']['Name'] 
        print('   Begin start status: {}'.format(ARK_STATUS))

    ARK_PUBLIC_DNS=result['Reservations'][0]['Instances'][0]['PublicDnsName']

    while ARK_STATUS != 'ok':
       time.sleep(15)
       result = ec2Client.describe_instance_status(InstanceIds=[ARK_CONSOLE_INSTANCE])
       ARK_STATUS=result['InstanceStatuses'][0]['InstanceStatus']['Status']
       print('   Final start status: {}'.format(ARK_STATUS))

    print('''
    ARK has been started.  Here is what you need to do to access it:

    1)  Open https://''' + ARK_PUBLIC_DNS + ''' in a browser and proceed through the security warning.

    2)  Register a new user and use this as the ARN:  ''' + arkUserRoleArn + '''
	
    3)  After registering, login with the user you just registered.

    4)  Click on the admin tab and add a "Server" for CentOS 7 with user of "centos"
        and an AMI for your ''' + os.environ["ARK_REGION"] + ''' region as show on 
        the Manual Launch tab here:
        https://aws.amazon.com/marketplace/fulfillment?productId=b7ee8a69-ee97-4a49-9e68-afaee216db2e

    5)  Click on one of the C7 DB Engines in the Admin tab, ensure the server you created
        is selected, ensure YUM credentials added if EPAS, and deselect the disabled checkbox.

    6)  Click the Clusters tab and launch a new cluster.
    \n''')

#########################################################################
##
## Run the main function 
##
#########################################################################
import boto3
import time
import sys
import textwrap

main()
