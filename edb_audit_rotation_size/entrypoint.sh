#!/bin/bash -l
echo ""
echo "***********************************************************************************"
echo "***********************************************************************************"
echo ""
echo "Running edb_audit_rotation_size test entrypoint script"
echo "..."

cd ~/9.6

echo ""
echo "###################################################################################"
echo "# Create a cluster, data on 5444"
echo "#"
echo "###################################################################################"
/usr/edb/as9.6/bin/initdb -D /var/lib/edb/as9.6/data 

echo ""
echo "###################################################################################"
echo "# Configure EDB Audit settings"
echo "###################################################################################"
sed -i "s/#edb_audit = 'none'/edb_audit = 'xml'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_directory/edb_audit_directory/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_statement = 'ddl, error'/edb_audit_statement = 'all'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_connect = 'failed'/edb_audit_connect = 'all'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_disconnect = 'none'/edb_audit_disconnect = 'all'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_tag = ''/edb_audit_tag = 'Testing EDB Audit rotation size settings using default filename'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_rotation_size = 0/edb_audit_rotation_size = 1/" /var/lib/edb/as9.6/data/postgresql.conf
#sed -i "s/#edb_audit_filename = 'audit-%Y-%m-%d_%H%M%S'/edb_audit_filename = 'audit'/" /var/lib/edb/as9.6/data/postgresql.conf


echo ""
echo "###################################################################################"
echo "# Show EDB Audit settings and other configuration parameters in postgresql.conf"
echo "###################################################################################"
grep "port =" /var/lib/edb/as9.6/data/postgresql.conf 
grep edb_audit /var/lib/edb/as9.6/data/postgresql.conf



echo ""
echo "###################################################################################"
echo "# Start the cluster "
echo "###################################################################################"
/usr/edb/as9.6/bin/pg_ctl -wD /var/lib/edb/as9.6/data start


/usr/edb/as9.6/bin/psql -d edb -U enterprisedb -p 5444 -h localhost -f /usr/edb/as9.6/share/edb-sample.sql

echo ""
echo "###################################################################################"
echo "# Initialize pgbench "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -i -d edb -U enterprisedb -p 5444 -h localhost

echo ""
echo "###################################################################################"
echo "# Show listing of audit-*.xml files with size "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit-*.xml

echo ""
echo "###################################################################################"
echo "# Perform pgbunch run 1 "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -C -c 25 -s 100 edb

echo ""
echo "###################################################################################"
echo "# Show listing of audit-*.xml file with size "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit-*.xml

echo ""
echo "###################################################################################"
echo "# Perform pgbunch run 2 "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -C -c 25 -s 100 edb

echo ""
echo "###################################################################################"
echo "# Show listing of audit-*.xml file with size "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit-*.xml

echo ""
echo "###################################################################################"
echo "# Perform pgbunch run 3 "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -C -c 25 -s 100 edb

echo ""
echo "###################################################################################"
echo "# Show listing of audit.xml file with size "
echo "# Note that audit-*.xml files are rotated at configured rotation size of 1 Mb "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit-*.xml

echo ""
echo "###################################################################################"
echo "# Set edb_audit_filename to 'audit', update edb_audit_tag parameter, and reload configuration "
echo "###################################################################################"
/usr/edb/as9.6/bin/psql -d edb -U enterprisedb -p 5444 -h localhost -c "ALTER SYSTEM SET edb_audit_filename TO 'audit'"
/usr/edb/as9.6/bin/psql -d edb -U enterprisedb -p 5444 -h localhost -c "ALTER SYSTEM SET edb_audit_tag TO 'Testing EDB Audit rotation size settings using new audit filename'"
/usr/edb/as9.6/bin/psql -d edb -U enterprisedb -p 5444 -h localhost -c "select pg_reload_conf()"


echo ""
echo "###################################################################################"
echo "# Show listing of audit.xml file with size "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit.xml

echo ""
echo "###################################################################################"
echo "# Perform pgbunch run 4 "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -C -c 25 -s 100 edb

echo ""
echo "###################################################################################"
echo "# Show listing of audit.xml file with size "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit.xml

echo ""
echo "###################################################################################"
echo "# Perform pgbunch run 5 "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -C -c 25 -s 100 edb

echo ""
echo "###################################################################################"
echo "# Show listing of audit.xml file with size "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit.xml

echo ""
echo "###################################################################################"
echo "# Perform pgbunch run 6 "
echo "###################################################################################"
/usr/edb/as9.6/bin/pgbench -C -c 25 -s 100 edb

echo ""
echo "###################################################################################"
echo "# Show listing of audit.xml file with size "
echo "# Note that audit.xml file not rotated at configured rotation size of 1 Mb "
echo "###################################################################################"
ls -al /var/lib/edb/as9.6/data/edb_audit/audit.xml

#Uncomment the following to enter into docker instance for further inspection if desired
echo ""
echo "Opening bash prompt"
echo "..."
/bin/bash -l

echo ""
echo "Exiting edb_audit_rotation_size test entrypoint script"
echo ""
