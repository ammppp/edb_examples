#!/bin/bash

#
# Start the DB under the PGaaS
#
/usr/ppas-9.5/bin/pg_ctl -wD /var/lib/ppas/9.5/data start

#
# Start the httpd server (would normally be PEM)
#
sudo /usr/sbin/httpd -DFOREGROUND
