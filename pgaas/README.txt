
Steps to use:

git clone https://ammppp@bitbucket.org/ammppp/edb_examples.git
cd edb_examples/pgaas
export EDB_YUM_USERNAME=<user>
export EDB_YUM_PASSWORD=<password>
make build
make run
# 1) point a browser at http://locahost:8080/pgaas
# 2) register as a first time user
# 3) logout
# 4) login as the user you registered as
# 5) click database tab and create database
# 6) click manage tab and link for web based pgadmin
# 7) click the PPAS database link
# 8) login as user in #2 and #4

