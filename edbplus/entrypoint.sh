#!/bin/bash

# Start the database and run a script via psql and sqlplus
/usr/edb/as9.6/bin/pg_ctl -wD /var/lib/edb/as9.6/data start 

# Create a test script 
echo -e "select version();" > /tmp/test.sql 

# Run the script via psql 
echo -e "\n\n\nPSQL Test:\n\n\n"
psql -d edb -h 127.0.0.1 -f /tmp/test.sql

# Hack the edbplus.sh file to add SSL options
sudo sed -i "s/runJREApplication/runJREApplication \$SSL_OPTS/" /usr/edb/as9.6/edbplus/edbplus.sh

# Run the script via sqlplus 
echo -e "\n\n\nEDBPlus Test:\n\n\n"
export SSL_OPTS="$SSL_OPTS -Djavax.net.ssl.keyStore=/var/lib/edb/.postgresql/enterprisedb.jks" 
export SSL_OPTS="$SSL_OPTS -Djavax.net.ssl.keyStorePassword=enterprisedb-key-pass" 
export SSL_OPTS="$SSL_OPTS -Djavax.net.ssl.trustStore=/var/lib/edb/.postgresql/ca.keystore" 
export SSL_OPTS="$SSL_OPTS -Djavax.net.ssl.trustStorePassword=ca-key-pass" 
echo "exit\n" | /usr/edb/as9.6/edbplus/edbplus.sh -s enterprisedb/junkpass@127.0.0.1:5444/edb?ssl=true @/tmp/test.sql 

# Shutdown the database 
/usr/edb/as9.6/bin/pg_ctl -mf -D /var/lib/edb/as9.6/data stop



