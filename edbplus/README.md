# EDB Example using EDBPlus client cert authentication  with EPAS 9.6

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/edbplus

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build edbplus 
docker-compose run edbplus 
```
