# EDB Example with of pg_upgrade from EPAS 9.5 to 9.6 

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/pg_upgrade

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build pgupgrade
docker-compose run pgupgrade 

# Watch the error output 
```
