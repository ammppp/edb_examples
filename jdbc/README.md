# EDB Example using JDBC with EPAS 9.6

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/jdbc

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build jdbc 
docker-compose run jdbc 

# Enter SQL statements at the prompt like this:

Enter SQL: 
select * from dual
dummy = X

Enter SQL: 
create table person (first text, last text) 

Enter SQL: 
insert into person values ('John', 'Doe');

Enter SQL: 
insert into person values ('Jane', 'Smith');

Enter SQL: 
select * from person
first = John,  last = Doe
first = Jane,  last = Smith
 
```
