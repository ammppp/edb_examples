package test;

import java.sql.*;
import javax.sql.*;
import java.util.*;

public class JdbcTest {

   public static void main(String[] args) throws Exception {

      Connection c = DriverManager.getConnection("jdbc:edb://localhost:5444/edb", "enterprisedb", "enterprisedb");

      Scanner reader = new Scanner(System.in);  // Reading from System.in

      while (true) {
          System.out.print("\nEnter SQL: ");
          String line = reader.nextLine(); 
      
          Statement s = c.createStatement();
          s.execute(line);
          ResultSet resultSet = s.getResultSet(); 
          while (resultSet != null && resultSet.next()) {
             ResultSetMetaData rsmd = resultSet.getMetaData();
             int columnsNumber = rsmd.getColumnCount();
             for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = resultSet.getString(i);
                System.out.print(rsmd.getColumnName(i) + " = " + columnValue);
             }
             System.out.println("");
          }
      }
   }

}
