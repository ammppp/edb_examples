# EDB Example with EPAS 9.6, pgPool, Tomcat, and pg trgm for fuzzy searches

This Docker container deploys
* EPAS 9.5 master and replica
* Sample table called "book" that ingest lines of a book, one line per row
* pgPool for load balancing
* Tomcat with a thread pool to the load balancer

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/tomcat_pgpool

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build tomcat
docker-compose run --service-ports tomcat

# Open this link in a browser:  http://localhost:8080/test/test.jsp?threashold=0.5&word=artistik
```
