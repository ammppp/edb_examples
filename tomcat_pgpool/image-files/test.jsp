<%@ page language="java" import="javax.naming.*,javax.sql.*,java.sql.*" %>

<HTML>
<BODY>
<% 
   InitialContext cxt = new InitialContext();
   DataSource ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/TestDB" );

   // ds.getConnection() will always pull the last connection that was closed.
   // So, if you do all of these steps in a single loop, you will alway use the
   // same connection.
   Connection conn1 = ds.getConnection();
   Connection conn2 = ds.getConnection();
   Connection conn3 = ds.getConnection();
   Connection conn4 = ds.getConnection();
   Connection conn5 = ds.getConnection();

   Statement statement1 = conn1.createStatement();
   Statement statement2 = conn2.createStatement();
   Statement statement3 = conn3.createStatement();
   Statement statement4 = conn4.createStatement();
   Statement statement5 = conn5.createStatement();

   ResultSet rs1 = statement1.executeQuery("select inet_server_port()");
   ResultSet rs2 = statement2.executeQuery("select inet_server_port()");
   ResultSet rs3 = statement3.executeQuery("select inet_server_port()");
   ResultSet rs4 = statement4.executeQuery("select inet_server_port()");
   ResultSet rs5 = statement5.executeQuery("select inet_server_port()");

   while(rs1.next()){ out.println("Connection1 " + conn1 + " to DB on port: " + rs1.getString(1) + "<br>"); }
   while(rs2.next()){ out.println("Connection2 " + conn2 + " to DB on port: " + rs2.getString(1) + "<br>"); }
   while(rs3.next()){ out.println("Connection3 " + conn3 + " to DB on port: " + rs3.getString(1) + "<br>"); }
   while(rs4.next()){ out.println("Connection4 " + conn4 + " to DB on port: " + rs4.getString(1) + "<br>"); }
   while(rs5.next()){ out.println("Connection5 " + conn5 + " to DB on port: " + rs5.getString(1) + "<br>"); }
   
   rs1.close();
   rs2.close();
   rs3.close();
   rs4.close();
   rs5.close();

   statement1.close();
   statement2.close();
   statement3.close();
   statement4.close();
   statement5.close();

   conn1.close();
   conn2.close();
   conn3.close();
   conn4.close();
   conn5.close();

   out.println("<br><br>");

   String threshold = request.getParameter("threshold");
   if(threshold == null) {threshold = "0.5";}

   String searchWord = request.getParameter("word");
   if(searchWord == null) {searchWord = "paenting";}

   Connection conn = ds.getConnection();
   Statement statement = conn.createStatement();
   statement.execute("set pg_trgm.word_similarity_threshold="+threshold+";");
   ResultSet rs = statement.executeQuery(
      "SELECT inet_server_port(), line, word_similarity('"+searchWord+"', line) AS sml " +
      "FROM book " +
      "WHERE '"+searchWord+"' <% line " +
      "ORDER BY sml DESC, line;");

   out.println("Change the search by appending ?threashold=0.5&word=artistik to the URL.<br>");
   out.println("Search results for minimum threshold " + threshold + " and query word \"" + searchWord + "\"<br><br>");
   while(rs.next()) {
      out.println(rs.getString(1) + " - " + rs.getString(3) + " - " + rs.getString(2) + "<br>");
   }

   rs.close();
   statement.close();
   conn.close();

%>
</BODY>
</HTML>
