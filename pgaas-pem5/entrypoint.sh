#!/bin/bash

#
# Start the PEM reository, agent, and web server
#
sudo service ppas-9.4 start
sudo service pemagent start
sudo service EnterpriseDBApachePhp start 

#
# Sleep forever to keep the container running...
#
while true; do
  sleep 30;
done
