# EDB Example using PGaaS on PEM 5 with EPAS 9.4 

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/pgaas-pem5

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>
export EDB_PORTAL_USERNAME=<your edb support portal username>
export EDB_PORTAL_PASSWORD=<your edb support portal password>

docker-compose build pgaas-pem5 
docker-compose run -d --service-ports pgaas-pem5
```

Now you can access these in a browser on your local machine:

* http://localhost:8080/pem (login as enterprisedb/enterprisedb)
* http://localhost:8080/pgaas (register, then logout, then login to access all tabs)
* http://localhost:8080/pgaas/phppgadmin (login as enterprisedb/enterprisedb or a username/password creeated via PGaaS)
