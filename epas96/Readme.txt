#
# Pre-requisites:
#   - Docker installed and running
#
# Demo Script:
#
#  In a terminal window
#

# set required variables
   export EDB_YUM_USERNAME=<username>
   export EDB_YUM_PASSWORD=<password>

# build the docker image
   docker-compose build epas96
   
# run the docker image
   docker-compose run epas96
   
