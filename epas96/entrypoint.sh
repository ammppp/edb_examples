#!/bin/bash -l
echo ""
echo "***********************************************************************************"
echo "***********************************************************************************"
echo ""
echo "Running epas96 entrypoint script"
echo "..."

cd ~/9.6

echo "#######################################################################"
echo "# Create a cluster, data on 5444"
echo "#"
echo "#######################################################################"
/usr/edb/as9.6/bin/initdb -D /var/lib/edb/as9.6/data 


echo "#######################################################################"
echo "# Configure the necessary settings to permit the clusters to stream logs to the Replication Server"
echo "#######################################################################"
#sed -i 's/#wal_level = minimal/wal_level = logical/' /var/lib/edb/as9.6/data/postgresql.conf 
#sed -i 's/#max_wal_senders = 0/max_wal_senders = 5/' /var/lib/edb/as9.6/data/postgresql.conf 
#sed -i 's/#max_replication_slots = 0/max_replication_slots = 5/' /var/lib/edb/as9.6/data/postgresql.conf 
#sed -i 's/#track_commit_timestamp = off/track_commit_timestamp = on/' /var/lib/edb/as9.6/data/postgresql.conf 
#sed -i 's/#host[ ]*replication/host replication/g' /var/lib/edb/as9.6/data/pg_hba.conf


echo "#######################################################################"
echo "# Start the master databases "
echo "#######################################################################"
/usr/edb/as9.6/bin/pg_ctl -wD /var/lib/edb/as9.6/data start


psql -d edb -U enterprisedb -p 5444 -h localhost -f /usr/edb/as9.6/share/edb-sample.sql

#echo "#######################################################################"
#echo "# Stop the EPAS cluster"
#echo "#######################################################################"
#/usr/edb/as9.6/bin/pg_ctl -mf -D /var/lib/edb/as9.6/data stop


echo "***********************************************************************************"
echo "***********************************************************************************"

#Uncomment the following to enter into docker instance for further inspection if desired
echo ""
echo "Opening bash prompt"
echo "..."
/bin/bash -l

echo ""
echo "Exiting epas96 entrypoint script"
echo ""

