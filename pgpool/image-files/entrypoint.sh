#!/bin/bash

pg_ctl -wD $PGDATA_MASTER start
pg_ctl -wD $PGDATA_REPLICA start

/usr/edb/pgpool3.5/bin/pgpool -D && sleep 1

echo -e "\n\n\nBeginning test...\n\n"

echo -e "TEST #1:  Notice that when we issue the same command from multiple psql sessions, some requests go to the master (5444) and some to the replica (5445) as expected...\n"

psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"
psql -d edb -p 9999 -c "select inet_server_port()"

echo -e "\n\nTEST #2:  Notice that when we issue the same command from a single psql session, the connection always gets routed to the same backend server, never load balanced to the other backend server...\n"

cat <<EOF > /tmp/test.sql
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
select inet_server_port();
EOF

psql -d edb -p 9999 -f /tmp/test.sql

echo -e "\n\nQUESTION:  In the second example, why does the single psql connection not load balance the queries like the first example does?\n"

