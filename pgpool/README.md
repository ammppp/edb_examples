# EDB Example with EPAS 9.6 and pgPool

This Docker container deploys
* EPAS 9.5 master and replica
* pgPool for load balancing

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/pgpool

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build pgpool
docker-compose run --service-ports pgpool

# Watch the examples the print out load balancing requests to pgpool
```
