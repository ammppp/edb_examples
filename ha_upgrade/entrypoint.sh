#!/bin/bash -l
echo ""
echo "Running HA Upgrade entrypoint script"
echo "..."

echo ""
echo "Create an EDB Postgres Advanced Server v9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/initdb -D /var/lib/ppas/9.5/data

echo ""
echo "Start the EDB Postgres Advanced Server v9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/pg_ctl -w -D /var/lib/ppas/9.5/data -l /var/lib/ppas/9.5/data/startup.log start

echo ""
echo "Give enterprisedb database user a password"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "CREATE USER enterprisedb IDENTIFIED BY enterprisedb"

echo ""
echo "Create a new database users in the 9.5 cluster to be used with sample database"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "CREATE USER srsample IDENTIFIED BY edb"
/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "CREATE USER jrsample IDENTIFIED BY edb"

echo ""
echo "Create a sample database in the 9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "CREATE DATABASE sample owner srsample"

echo ""
echo "Create sample1 schema in sample database in the 9.5 cluster"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d sample -U srsample -c "CREATE SCHEMA sample1"

/usr/ppas-9.5/bin/psql -p 5444 -d sample -U srsample -c "GRANT USAGE ON SCHEMA sample1 TO jrsample"

echo ""
echo "Set search_path for all users in sample database to sample1 schema"
echo "..."
/usr/ppas-9.5/bin/psql -p 5444 -d sample -c "ALTER ROLE ALL IN DATABASE sample SET search_path TO sample1;"

echo ""
echo "Install the EDB Sample database in the sample.sample1 schema"
echo ""
/usr/ppas-9.5/bin/psql -p 5444 -d sample -U srsample -f /usr/ppas-9.5/share/edb-sample.sql

#echo ""
#echo ".... List schemas in edb database"
#echo ""
#/usr/ppas-9.5/bin/psql -p 5444 -d edb -c "\dn"


echo "Create an EDB Postgres Advanced Server v9.6 cluster to serve as target of pg_upgrade"
echo "..."
/usr/edb/as9.6/bin/initdb -D /var/lib/edb/as9.6/data

echo ""
echo "Set port of 9.6 cluster to 5445"
echo "..."
sed -i 's/port = 5444/port = 5445/' /var/lib/edb/as9.6/data/postgresql.conf


#Uncomment the following to enter into docker instance for further inspection if desired
echo ""
echo "Opening bash prompt"
echo "..."
/bin/bash -l

echo ""
echo "Exiting HA Upgrade entrypoint script"
echo ""




