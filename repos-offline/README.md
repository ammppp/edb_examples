# EDB Example Cloning EDB Repositories for Offline Use

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/repos-offline

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build repos-offline 
docker-compose run repos-offline

# This will create a file called ./output/edb-repos.tar.gz

# Once you have that file, transfer it to the /tmp dir of some offline system
# and follow these steps below to create a local directory repo to install
# from.

# Become root user
sudo su -

# Extract the repo into / which will put it in /usr/edb/repo.  You don't
# have to put it here, but I think it is a good consistent location.  Keep in
# mind that this is not the "install" folder, this is just the directory that will
# hold the RPM repos.  It is you local equivalent to or copy of 
# http://yum.enterprisedb.com which you can't access, so you are 
# creating a directory based clone of that web repository.
tar -C / -xvf /tmp/edb-repos.tar.gz

# Install the RPM that installs EDB's GPG key and creates the edb.repo file
yum install -y /usr/edb/repos/edb-repos/edb-repo-9.6-4.noarch.rpm

# Update the edb.repo file to enable the repos we want and set the baseurl to
# our new directories of RPMs we created.  You can do this manually by
# editing /etc/yum.repos.d to set enabled=1 and correct baseurl or you can
# use these five sets of sed commands to do it automatically
sed -i "\/edb-repos/,/gpgcheck/ s,enabled=.*$,enabled=1," /etc/yum.repos.d/edb.repo 
sed -i "\/edb-repos/,/gpgcheck/ s,baseurl=.*$,baseurl=file:///usr/edb/repos/edb-repos," /etc/yum.repos.d/edb.repo 

sed -i "\/edbas96/,/gpgcheck/ s,enabled=.*$,enabled=1," /etc/yum.repos.d/edb.repo 
sed -i "\/edbas96/,/gpgcheck/ s,baseurl=.*$,baseurl=file:///usr/edb/repos/edbas96," /etc/yum.repos.d/edb.repo 

sed -i "\/enterprisedb-dependencies/,/gpgcheck/ s,enabled=.*$,enabled=1," /etc/yum.repos.d/edb.repo 
sed -i "\/enterprisedb-dependencies/,/gpgcheck/ s,baseurl=.*$,baseurl=file:///usr/edb/repos/enterprisedb-dependencies," /etc/yum.repos.d/edb.repo 

sed -i "\/enterprisedb-tools/,/gpgcheck/ s,enabled=.*$,enabled=1," /etc/yum.repos.d/edb.repo 
sed -i "\/enterprisedb-tools/,/gpgcheck/ s,baseurl=.*$,baseurl=file:///usr/edb/repos/enterprisedb-tools," /etc/yum.repos.d/edb.repo 

sed -i "\/enterprisedb-xdb60/,/gpgcheck/ s,enabled=.*$,enabled=1," /etc/yum.repos.d/edb.repo
sed -i "\/enterprisedb-xdb60/,/gpgcheck/ s,baseurl=.*$,baseurl=file:///usr/edb/repos/enterprisedb-xdb60," /etc/yum.repos.d/edb.repo

# Run this command, just to look at the edb.repo file to see what was enabled and
# see the baseurls set to the RPM directories that we took out of the tar.gz file
cat /etc/yum.repos.d/edb.repo

# That's it, now it's time to install..

# Let's do xdb first...
yum install -y ppas-xdb

# Run this command to see where xdb 6.1 was installed
ls /usr/ppas-xdb-6.1

# Now, let's do epas 9.6
yum install -y edb-as96-server

# Run this command to see where epas 9.6 was installed
ls /usr/edb/as9.6

# Et voila, you have installed xdb 6.1 and epas 9.6, whoohoo!

# Starting xdb 6.1 is like this (Java must be installed, which it should be in your VMs)
systemctl start edb-xdbpubserver.service
ps -ef | grep pubserver

# Here is how to initdb and start epas 9.6 by default if you want to do it the normal linux way
# Copy the default service configuration to /etc as shown below
cp /lib/systemd/system/edb-as-9.6.service /etc/systemd/system/edb-as-9.6.service

# Edit edb-as-9.6.server to set "Environment=PGDATA=..." to the 
# PGDATA dir you want (if not the default)
vi /etc/systemd/system/edb-as-9.6.service

# Create a new cluster dir
/usr/edb/as9.6/bin/edb-as-96-setup initdb

# Start the new cluster
systemctl start edb-as-9.6.service
ps -ef | grep post

# Boom, that's it, success!!!
 
```
