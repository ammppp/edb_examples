-- Create a job that will run every minute of every hour of every day
EXEC 
DBMS_SCHEDULER.CREATE_JOB(
  job_name => 'insert_test_job'
, job_type => 'PLSQL_BLOCK'
, job_action => 'BEGIN INSERT INTO job_test values ( sysdate, ''Job Created Entry @ ''||sysdate ); END;'
, start_date => NULL -- start job immediately
, repeat_interval => 'FREQ=MINUTELY;'||
                     'BYDAY=1,2,3,4,5,6,7;'|| -- run everyday of week
                     'BYHOUR=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;'|| -- run every hour of the day
                     'BYMINUTE=0,1,2,3,4,5,6,7,8,9'||
                             ',10,11,12,13,14,15,16,17,18,19'||
                             ',20,21,22,23,24,25,26,27,28,29'||
                             ',30,31,32,33,34,35,36,37,38,39'||
                             ',40,41,42,43,44,45,46,47,48,49'||
                             ',50,51,52,53,54,55,56,57,58,59;' -- run every minute
, end_date => NULL -- keep running job until disabled or dropped
, enabled  => TRUE -- enable job to run
, comments => 'This job is used to test the job scheduler.'
);
