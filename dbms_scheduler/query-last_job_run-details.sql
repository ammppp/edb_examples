\pset pager off
--\pset border 1
\x

WITH dt AS ( SELECT now current_date_time )
   , jt AS ( SELECT *
               FROM job_test
              WHERE label LIKE 'Job Created Entry %'
             ORDER BY create_dt DESC LIMIT 1
            )
SELECT dj.run_count       "dba_scheduler_jobs.run_count"
     , dj.failure_count   "dba_scheduler_jobs.failure_count"
     , dj.last_start_date "dba_scheduler_jobs.last_start_date"
     , dj.next_run_date   "dba_scheduler_jobs.next_run_date"
     , NULL               " "
     , dt.current_date_time
     , NULL               " "
     , jt.create_dt       "job_test.create_dt"
     , jt.label           "job_test.label"
  FROM dt
     , dba_scheduler_jobs dj LEFT OUTER JOIN jt ON 1=1
;
