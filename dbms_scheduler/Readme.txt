
Pre-requisites:
   - Docker installed and running
   - Git command line installed

Demo Script:

  In a terminal window
   cd ~
   mdkir docker
   cd docker
   git clone https://mlewandowski15@bitbucket.org/ammppp/edb_examples.git
   cd edb_examples
   git pull
   cd dbms_scheduler
   make alias  #then enter your password
   export EDB_YUM_USERNAME=<username>
   export EDB_YUM_PASSWORD=<password>
   make build  # this builds the docker image
   clear
   make run    # this creates/recreates the container, which runs the demo
