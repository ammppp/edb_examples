# EDB Example using PEM 7 with EPAS 9.6 

## Prerequisites

* Have Git and Docker installed on your local workstation

## Launch Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/pem7

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>
export EDB_PORTAL_USERNAME=<your edb support portal username>
export EDB_PORTAL_PASSWORD=<your edb support portal username>

docker-compose build pem7
docker-compose run -d --service-ports pem7
```

Now you can access these in a browser on your local machine:

* http://localhost:8080/pem (login as enterprisedb/enterprisedb, change host to localhost on database if it won't connect)
