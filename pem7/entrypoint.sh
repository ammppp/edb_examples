#!/bin/bash

#
# Start the PEM reository, agent, and web server
#
sudo service edb-as-9.6 start
sudo service pemagent start
sudo service PEMHTTPD start 

#
# Sleep forever to keep the container running...
#
while true; do
  sleep 30;
done
