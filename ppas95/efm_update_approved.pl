use List::MoreUtils 'any';

while (true) {
   my $approved = `sudo /usr/efm-2.0/bin/efm cluster-status efm | awk '/Allowed node host list/{getline; print}'`;
   $approved =~ s/^\s+|\s+$//g;
   #print "Approved: $approved \n";

   my $standbys = `psql -c "select client_addr from pg_stat_replication" | grep "\\." | xargs`;
   $standbys =~ s/^\s+|\s+$//g;
   #print "Standbys: $standbys \n";
   my @approvedList = split / /, $approved;
   my @standbysList = split / /, $standbys;

   foreach $ip (@standbysList){
      my $exists = any { $_ eq $ip } @approvedList;
      if ($exists == false) {
         print "Adding $ip to EFM approved list... \n";
         system("sudo /usr/efm-2.0/bin/efm add-node efm $ip");
      }
   }
   sleep 5;
}

