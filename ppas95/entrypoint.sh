#!/bin/bash -l

EFMHOST=`hostname -i`
export EDB_EFM_PASSWORD=replication
EFMPASSWORD=`perl /efm_password.pl | awk -F  "=" '/db.password/ {print $2}'`
sudo sed -i 's/bind.address=$/bind.address='"$EFMHOST"':5555/' /etc/efm-2.0/efm.properties 
sudo sed -i 's/db.password.encrypted=$/db.password.encrypted='"$EFMPASSWORD"'/' /etc/efm-2.0/efm.properties 


case "$1" in
"-replicates")
  rm -rf /var/lib/ppas/9.5/data/*
  shift
  IFS=':' read -a REPLICA <<< "$1"
  /usr/ppas-9.5/bin/pg_basebackup -U replication \
    -h ${REPLICA[0]} -p ${REPLICA[1]} \
    --xlog-method=stream \
    --pgdata=$PGDATA \
    --checkpoint=fast \
    --write-recovery-conf \
     --verbose \
      >> /var/lib/ppas/9.5/basebackup.log 
  echo "trigger_file = '/var/lib/ppas/9.5/data/trigger.file'" >> /var/lib/ppas/9.5/data/recovery.conf
  echo "recovery_target_timeline='latest'" >> /var/lib/ppas/9.5/data/recovery.conf 
  echo "restore_command = 'cp /pg_archive/%f %p'" >> /var/lib/ppas/9.5/data/recovery.conf
  sudo bash -c "echo ' ${REPLICA[0]}:5555 ' >> /etc/efm-2.0/efm.nodes"
  NODES=`psql -U replication -h ${REPLICA[0]} -p ${REPLICA[1]} -c "select client_addr || ':5555' from pg_stat_replication;" | grep ":5555" | sed 's/\/32//g'`
  sudo sudo bash -c "echo '$NODES' >> /etc/efm-2.0/efm.nodes"
  shift
  ;;
*)
  echo "I'm the master"
  ;;
esac

umask 022
/usr/ppas-9.5/bin/pg_ctl -w -D /var/lib/ppas/9.5/data start
perl efm_update_approved.pl &
perl start_efm.pl &
exec "$@"
