use strict;
use Expect;

my $timeout = 10; 
my $password =  $ENV{'EDB_EFM_PASSWORD'} . "\n";
my $command = 'sudo -u efm /usr/efm-2.0/bin/efm encrypt efm';
# Create the Expect object

my $exp = Expect->spawn($command) or die "Cannot spawn efm command \n";

$exp->expect($timeout, ["Please enter the password and hit enter: "]); 
$exp->send($password);
$exp->expect($timeout, ["Please enter the password again to confirm: "]);
$exp->send($password);

#$exp->expect($timeout, ["mpp#"]);
#$exp->send("exit\n"); 
#$exp->send("logout\n");
$exp->soft_close();
