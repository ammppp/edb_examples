#
#Pre-requisites:
#   - Docker installed and running
#
#Demo Script:
#
#  In a terminal window
#

# set required variables
   export EDB_YUM_USERNAME=<username>
   export EDB_YUM_PASSWORD=<password>

# build the docker image
   docker-compose build switchover_switchback
   
# run the docker image
   docker-compose run switchover_switchback
   
