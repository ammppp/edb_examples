#!/bin/bash -l
echo ""
echo "***********************************************************************************"
echo "***********************************************************************************"
echo ""
echo "Running Streaming Replication Switchover-Switchback entrypoint script"
echo "..."

cd ~/9.4

echo "***********************************************************************************"
echo ""
echo "1 Set up the databases for the test"
echo ""
echo "1.1 Create the initial Primary EPAS 9.4 database cluster via intidb command"
/usr/ppas-9.4/bin/initdb -D /var/lib/ppas/9.4/data_a

echo ""
echo "1.1.2 Set postgresql.conf configuration parameters to support streaming replication using a replication slot"
sed -i 's/#wal_level = minimal/wal_level = logical/' /var/lib/ppas/9.4/data_a/postgresql.conf
sed -i 's/#max_wal_senders = 0/max_wal_senders = 3/' /var/lib/ppas/9.4/data_a/postgresql.conf 
sed -i 's/#max_replication_slots = 0/max_replication_slots = 3/' /var/lib/ppas/9.4/data_a/postgresql.conf
sed -i 's/#hot_standby = off/hot_standby = on/' /var/lib/ppas/9.4/data_a/postgresql.conf
sed -i 's/#wal_keep_segments = 0/wal_keep_segments = 20/' /var/lib/ppas/9.4/data_a/postgresql.conf # may not need this

echo ""
echo "1.1.2.1 Show parameters configured in postgresql.conf file"
grep "port =" /var/lib/ppas/9.4/data_a/postgresql.conf 
grep wal_level /var/lib/ppas/9.4/data_a/postgresql.conf
grep max_wal_senders /var/lib/ppas/9.4/data_a/postgresql.conf
grep max_replication_slots /var/lib/ppas/9.4/data_a/postgresql.conf
grep "hot_standby =" /var/lib/ppas/9.4/data_a/postgresql.conf
grep wal_keep_segments /var/lib/ppas/9.4/data_a/postgresql.conf

echo ""
echo "1.1.3 Authorize replication connections in pg_hba.conf"
sed -i 's/#host[ ]*replication/host replication/g' /var/lib/ppas/9.4/data_a/pg_hba.conf

echo ""
echo "1.1.3.1 Run tail to show end of pg_hba.conf file"
tail /var/lib/ppas/9.4/data_a/pg_hba.conf

echo ""
echo "1.1.4 Start the initial primary EPAS v9.4 cluster"
/usr/ppas-9.4/bin/pg_ctl -w -D /var/lib/ppas/9.4/data_a -l /var/lib/ppas/9.4/data_a/startup.log start

echo ""
echo "1.1.5 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "1.1.6 Give enterprisedb database user a password"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "ALTER USER enterprisedb IDENTIFIED BY enterprisedb"

echo ""
echo "1.1.7 Create a replication slot ['s2_slot'] in Primary database for standby database"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_create_physical_replication_slot('s2_slot')"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_replication_slots"

echo ""
echo "1.2 Create the initial Standby EPAS 9.4 database cluster via a pg_basebackup of primary cluster"
/usr/ppas-9.4/bin/pg_basebackup -D /var/lib/ppas/9.4/data_b -p 5444 -h 127.0.0.1

echo ""
echo "1.2.1 Set port of Standby database to port 5445"
sed -i 's/port = 5444/port = 5445/' /var/lib/ppas/9.4/data_b/postgresql.conf

echo ""
echo "1.2.2 Create recovery.conf file in Standby instance and show contents"
cat <<EOF > /var/lib/ppas/9.4/data_b/recovery.conf
recovery_target_timeline = 'latest'
standby_mode = 'on'
primary_conninfo = 'user=enterprisedb host=127.0.0.1 port=5444 sslmode=prefer sslcompression=1'
primary_slot_name = 's2_slot'
trigger_file = 's2_trigger.file'
EOF

cat /var/lib/ppas/9.4/data_b/recovery.conf

echo ""
echo "1.2.3 Start the initial secondary EPAS v9.4 cluster"
/usr/ppas-9.4/bin/pg_ctl -w -D /var/lib/ppas/9.4/data_b -l /var/lib/ppas/9.4/data_b/startup.log start

echo ""
echo "1.2.4 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "1.3 Test the primary database and standby databases"
echo "1.3.1 Test the primary cluster [port 5444]"
echo "1.3.1.1 Create my_table in edb database in primary cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "CREATE TABLE my_table ( my_col text )"

echo ""
echo "1.3.1.2 List tables in edb database in primary cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "\dt"

echo ""
echo "1.3.1.3 Insert into my_table in edb database in primary cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "INSERT INTO my_table VALUES (E'Hello World\!')"

echo ""
echo "1.3.1.4 Query my_table in edb database in primary cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "TABLE my_table"

# may need sleep here
sleep 2

echo ""
echo "1.3.2 Test the standby cluster [port 5445]"
echo "1.3.2.1 List tables in edb database in standby [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "\dt"

echo ""
echo "1.3.2.2 Query my_table in edb database in standby [Port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo ""
echo "1.3.2.3 Attempt to insert into my_table in edb database in standby [Port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "INSERT INTO my_table VALUES (E'Well, hello there\!')"

echo ""
echo "1.3.1.4 Create my_table in edb database in standby cluster [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "CREATE TABLE my_table2 ( my_col text )"

echo ""
echo "1.3.2.5 List tables in edb database in standby [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "\dt"

echo ""
echo "1.3.2.6 Query my_table in edb database in standby [Port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo "***********************************************************************************"
echo ""
echo ""
echo "2 Perform a Switchover (i.e., make current Standby the Primary and current Primary the Standby"
echo "... 2.1 Do clean shutdown of primary cluster [port 5444] (-m fast or smart)"
/usr/ppas-9.4/bin/pg_ctl -D /var/lib/ppas/9.4/data_a stop -mf


echo ""
echo "...... 2.1.1 List running 'edb-post' processes"
ps -ef | grep edb-post


echo ""
echo "... 2.2 Check for sync status and recovery status of standby cluster [port 5445] before promoting it." # this may not be necessary
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c 'select pg_last_xlog_receive_location() "receive_location", pg_last_xlog_replay_location() "replay_location", pg_is_in_recovery() "recovery_status";'

echo ""
echo "receive_location and replay_location should match, recovery_status = t"


echo ""
echo "... 2.3 Make the current standby [port 5445] as new primary by running pg_ctl promote or creating a trigger file."

echo ""
echo "...... 2.3.1 show trigger_file setting in recovery.conf file in current standby [port 5445]"
grep trigger_file /var/lib/ppas/9.4/data_b/recovery.conf

echo ""
echo "...... 2.3.2 touch the trigger file"
touch /var/lib/ppas/9.4/data_b/s2_trigger.file

echo "...... 2.3.3 ls showing trigger file"
ls /var/lib/ppas/9.4/data_b/*trigger*

echo ""
echo "* Note: could have used \"pg_ctl -D data_b promote\" instead of a trigger file"

# may need sleep here
sleep 5

echo ""
echo "...... 2.3.4 Check if the old standby is still in recovery"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "select pg_is_in_recovery();"

echo ""
echo "result should be pg_is_in_recovery = f"

echo ""
echo "...... 2.3.5 Check latest database log if desired to see that Standby has been successfully promoted a master"
ls -lrt -d /var/lib/ppas/9.4/data_b/pg_log/*.log | tail -n1
cat "$(ls -rt -d /var/lib/ppas/9.4/data_b/pg_log/*.log | tail -n1)"

echo ""
echo "... 2.4 Create  a replication slot ['s1_slot'] in new primary cluster [port 5445] for new standby cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "SELECT * FROM pg_create_physical_replication_slot('s1_slot')"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "SELECT * FROM pg_replication_slots"

echo ""
echo "... 2.5 Create/update \"recovery.conf\" file on old primary [port 5444], including by setting \"recovery_target_timline='latest'\""
cat <<EOF > /var/lib/ppas/9.4/data_a/recovery.conf
recovery_target_timeline = 'latest'
standby_mode = 'on'
primary_conninfo = 'user=enterprisedb host=127.0.0.1 port=5445 sslmode=prefer sslcompression=1'
primary_slot_name = 's1_slot'
trigger_file = 's1_trigger.file'
EOF

cat /var/lib/ppas/9.4/data_a/recovery.conf

echo ""
echo "... 2.6 Start old primary cluster [port 5444] as new standby"
/usr/ppas-9.4/bin/pg_ctl -w -D /var/lib/ppas/9.4/data_a -l /var/lib/ppas/9.4/data_a/data_a_standby_startup.log start

echo ""
echo "...... 2.6.1 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "...... 2.6.2 Check latest database log"
ls -lrt -d /var/lib/ppas/9.4/data_a/pg_log/*.log | tail -n1
cat "$(ls -rt -d /var/lib/ppas/9.4/data_a/pg_log/*.log | tail -n1)"


echo ""
echo "...... 2.6.3 Verify the new Standby recovery status."
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "select pg_is_in_recovery();"

echo ""
echo "result should be pg_is_in_recovery = t"

echo ""
echo "... 2.7 Test primary and standby databases"
echo ""
echo "...... 2.7.1 Test the new primary cluster [port 5445]"
echo "......... 2.7.1.1 Insert data into my_table test table on new primary cluster [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "INSERT INTO my_table VALUES (E'Hola Mundo\!')"

echo ""
echo "......... 2.7.1.2 Query my_table test table on new primary cluster [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "TABLE my_table"

# may need sleep here
sleep 2

echo ""
echo "...... 2.7.2 Test the new standby cluster [port 5444]"
echo "......... 2.7.2.1 List tables in edb database on new standby database [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "\dt"

echo ""
echo "......... 2.7.2.2 Query my_table on new standby database [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "TABLE my_table"

echo ""
echo "......... 2.7.2.3 Attempt to insert into my_table on new standby database [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "INSERT INTO my_table VALUES (E'Howdy\!')"

echo ""
echo "......... 2.7.2.4 Attempt to create a a new table on new standby database [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "CREATE TABLE my_table2 ( my_col text )"

echo ""
echo "......... 2.7.2.5 List tables in edb database on new standby database [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "\dt"

echo ""
echo "......... 2.7.2.6 Query my_table on new standby database [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "TABLE my_table"

echo "***********************************************************************************"
echo ""
echo "3 Perform a Switchback (i.e., make current Standby the Primary and current Primary the Standby"
echo "... 3.1 Do clean shutdown of current primary cluster [port 5445] (-m fast or smart)"
/usr/ppas-9.4/bin/pg_ctl -D /var/lib/ppas/9.4/data_b stop -mf

echo ""
echo "...... 3.1.1 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "... 3.2 Check for sync status of current standby [on port 5444] before promoting. "
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c 'select pg_last_xlog_receive_location() "receive_location", pg_last_xlog_replay_location() "replay_location", pg_is_in_recovery() "recovery_status";'

echo ""
echo "receive_location and replay_location should match, recovery_status = t"

echo ""
echo "... 3.3 Make the current standby [port 5444] the new primary"
echo "...... using the pg_ctl promote command"
/usr/ppas-9.4/bin/pg_ctl -D /var/lib/ppas/9.4/data_a promote
#grep trigger_file /var/lib/ppas/9.4/data_a/recovery.conf
#touch /var/lib/ppas/9.4/data_a/s1_trigger.file

echo ""
echo "* Note: could have created the trigger file instead of using the promote command"

# may need sleep here
sleep 5

echo ""
echo "...... 3.3.1 Verify the primary cluster [port 5444] current recovery status."
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "select pg_is_in_recovery()"

echo ""
echo "result should be pg_is_in_recovery = f"

echo ""
echo "...... 3.3.2 Check latest database log if desired to see previous standby [on port 5444] has been successfully promoted a master"
ls -lrt -d /var/lib/ppas/9.4/data_a/pg_log/*.log | tail -n1
cat "$(ls -rt -d /var/lib/ppas/9.4/data_a/pg_log/*.log | tail -n1)"


#echo ""
#echo "...... reset s2_slot replication slot"
#/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_replication_slots"
#/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_drop_replication_slot('s2_slot')"
#/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_replication_slots"
#/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_create_physical_replication_slot('s2_slot')"
#/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "SELECT * FROM pg_replication_slots"


echo ""
echo "... 3.4. Configure previous primary [on port 5445] as new standby"
echo "...... 3.4.1 Create recovery.conf file in previous primary [on port 5445] as new standby."
mv /var/lib/ppas/9.4/data_b/recovery.done /var/lib/ppas/9.4/data_b/recovery.conf

echo ""
echo "...... 3.4.2 Restart stopped previous primary [5445] as new standby"
/usr/ppas-9.4/bin/pg_ctl -w -D /var/lib/ppas/9.4/data_b -l /var/lib/ppas/9.4/data_b/data_b_standby_startup_2.log start

echo ""
echo "...... 3.4.3 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "...... 3.4.4 Check latest database logs if desired to see that Standby [on port 5445] has been successfully restarted"
ls -lrt -d /var/lib/ppas/9.4/data_b/pg_log/*.log | tail -n1
cat "$(ls -rt -d /var/lib/ppas/9.4/data_b/pg_log/*.log | tail -n1)"


echo ""
echo "... 3.5 Test the primary and standby clusters"
echo ""
echo "...... 3.5 Test the primary cluster [port 5444]"
echo "......... 3.5.1 Insert data into my_table test table on new primary cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "INSERT INTO my_table VALUES (E'Howdy\!')"

echo ""
echo "......... 3.5.2 Query my_table test table on new primary cluster [port 5444]"
/usr/ppas-9.4/bin/psql -p 5444 -d edb -c "TABLE my_table"

# may need sleep here
sleep 2

echo ""
echo "...... 3.6 Test the standby cluster [port 5445]"
echo "......... 3.6.1 Query my_table on standby cluster [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "TABLE my_table"

echo ""
echo "......... 3.6.2 Attempt to insert into my_table on standby cluster [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "INSERT INTO my_table VALUES (E'Greetings\!')"

echo "......... 3.6.3 Query my_table on standby cluster [port 5445]"
/usr/ppas-9.4/bin/psql -p 5445 -d edb -c "TABLE my_table"


echo "***********************************************************************************"
echo "***********************************************************************************"

#Uncomment the following to enter into docker instance for further inspection if desired
#echo ""
#echo "Opening bash prompt"
#echo "..."
#/bin/bash -l

echo ""
echo "Exiting Streaming Replication Switchover-Switchback entrypoint script"
echo ""




