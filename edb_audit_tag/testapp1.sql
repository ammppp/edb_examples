\echo "Setting edb_audit_tag parameter"
SET edb_audit_tag TO 'Test App 1 | 192.168.0.100 | Matt Lewandowski | EnterpriseDB - Senior Sales Engineer';

\echo "Current edb_audit_tag value:"
SHOW edb_audit_tag;

\echo "Inserting data into app.parts table"
INSERT INTO app.parts
            ( system_name, part_name, part_function )
     VALUES ( 'Airplane', 'Fuselage', 'Hold Things Together - Carry Payload' )
          , ( 'Airplane', 'Cockpit', 'Command and Control' )
          , ( 'Airplane', 'Turbine Engine', 'Generate Thrust' )
          , ( 'Airplane', 'Wing', 'Generate Lift' )
          , ( 'Airplane', 'Winglet', 'Decrease Drag' )
          , ( 'Airplane', 'Horizontal Stabilizer', 'Control Pitch' )
          , ( 'Airplane', 'Vertical Stabilizer', 'Control Yaw' )
          , ( 'Airplane', 'Rudder', 'Change Yaw' )
          , ( 'Airplane', 'Elevator', 'Change Pitch' )
          , ( 'Airplane', 'Flaps', 'Increase Lift and Drag' )
          , ( 'Airplane', 'Aileron', 'Change Roll' )
          , ( 'Airplane', 'Spoiler', 'Change Lift, Drag, and Roll' )
          , ( 'Airplane', 'Slats', 'Increase Lift' )
;

\echo "Querying app.parts table"
SELECT * FROM app.parts ORDER BY system_name, part_name, id;