\echo "Creating testapp_admin User"
CREATE USER testapp_admin IDENTIFIED BY edb;

\echo "Creating testapp1_dbuser User"
CREATE USER testapp1_dbuser IDENTIFIED BY edb;

\echo "Creating testapp2_dbuser User"
CREATE USER testapp2_dbuser IDENTIFIED BY edb;


\echo "Creating app_data_manager and app_data_reviewer roles"
CREATE ROLE app_data_manager;
CREATE ROLE app_data_reviewer;

\echo "Granting roles to users"
GRANT app_data_manager TO testapp1_dbuser;
GRANT app_data_reviewer TO testapp2_dbuser;

\echo "Creating test Database"
CREATE DATABASE test OWNER testapp_admin;
