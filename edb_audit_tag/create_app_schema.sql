\echo "Setting edb_audit_tag parameter"
SET edb_audit_tag TO 'Executing script to create test app database objects';

\echo "Current edb_audit_tag value:"
SHOW edb_audit_tag;

\echo "Creating app schema"
CREATE SCHEMA app;

\echo "Creating app.parts table"
CREATE TABLE IF NOT EXISTS app.parts
( id            SERIAL PRIMARY KEY
, system_name   VARCHAR2(200)
, part_name     VARCHAR2(200)
, part_function TEXT
);

\echo "Creating app.get_score function"
CREATE OR REPLACE FUNCTION app.get_score( p_name VARCHAR2 )
  RETURN numeric
IS
  v_retval numeric := 0;
BEGIN
  v_retval := ROUND( random()::numeric*100, 1);
  RETURN v_retval;
END get_score;

\echo "Granting usage on app schema to roles"
GRANT USAGE ON SCHEMA app TO app_data_manager, app_data_reviewer;

\echo "Granting privileges on app.parts table to app_data_manager role"
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE ON app.parts TO app_data_manager;
GRANT USAGE ON app.parts_id_seq TO app_data_manager;

\echo "Granting privileges on app.parts table and app.get_score function to app_data_reviewer role"
GRANT SELECT ON app.parts TO app_data_reviewer;
GRANT EXECUTE ON FUNCTION app.get_score( p_name VARCHAR2 ) TO app_data_reviewer;
