\echo "Setting edb_audit_tag parameter"
SET edb_audit_tag TO 'App Identifier: Test App 2, App User: matthew.lewandowski@enterprisedb.com, Access Point: 172.31.12.107';

\echo "Current edb_audit_tag value:"
SHOW edb_audit_tag;

\echo "Selecting info from app.parts table and get_score function"
SELECT p.part_name
     , app.get_score( p.part_name ) average_interest_score
  FROM app.parts p
 WHERE system_name = 'Airplane'
ORDER BY 2 DESC, 1
;

\echo "Attempt to insert data into app.parts table (should result in error)"
INSERT INTO app.parts 
            ( system_name, part_name, part_function )
     VALUES ( 'Automobile', 'Steering Wheel', 'Used by driver to steer vehicle to the left or right.' );
     
     
\echo "Attempt to execute SELECT statement with non-existing identifier"
SELECT p.id, p.part_name, p.description FROM app.parts p;

\echo "Attempt to execute invalid SQL statement"
SELECT * app.parts;

