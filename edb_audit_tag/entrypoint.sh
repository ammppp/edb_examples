#!/bin/bash -l
echo ""
echo "***********************************************************************************"
echo "***********************************************************************************"
echo ""
echo "Running EDB Audit Tag example entry point script"
echo "..."

cd ~

echo "***********************************************************************************"
echo ""
echo ""
echo "1 Set up the EPAS instance"
echo ""
echo "1.1 Create an EPAS 9.6 database cluster via intidb command"
/usr/edb/as9.6/bin/initdb -D /var/lib/edb/as9.6/data

echo ""
echo "1.1.2 Set postgresql.conf configuration parameters to enable auditing"
sed -i "s/#edb_audit = 'none'/edb_audit = 'xml'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_directory = 'edb_audit'/edb_audit_directory = '\/edb_audit'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_statement = 'ddl, error'/edb_audit_statement = 'all'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_connect = 'failed'/edb_audit_connect = 'all'/" /var/lib/edb/as9.6/data/postgresql.conf
sed -i "s/#edb_audit_disconnect = 'none'/edb_audit_disconnect = 'all'/" /var/lib/edb/as9.6/data/postgresql.conf

echo ""
echo "1.1.2.1 Show parameters configured in postgresql.conf file"
grep "port =" /var/lib/edb/as9.6/data/postgresql.conf 
grep "edb_audit =" /var/lib/edb/as9.6/data/postgresql.conf
grep "edb_audit_directory =" /var/lib/edb/as9.6/data/postgresql.conf
grep "edb_audit_statement =" /var/lib/edb/as9.6/data/postgresql.conf
grep "edb_audit_connect =" /var/lib/edb/as9.6/data/postgresql.conf
grep "edb_audit_disconnect =" /var/lib/edb/as9.6/data/postgresql.conf
grep "edb_audit_tag =" /var/lib/edb/as9.6/data/postgresql.conf

#echo ""
#echo "1.1.3 Authorize replication connections in pg_hba.conf"
#sed -i 's/#host[ ]*replication/host replication/g' /var/lib/ppas/9.4/data_a/pg_hba.conf

echo ""
echo "1.1.3 Run tail to show end of pg_hba.conf file"
echo "...... connections should be configured for trust authentication"
tail /var/lib/edb/as9.6/data/pg_hba.conf

echo ""
echo "1.1.4 Start EPAS v9.6 cluster"
/usr/edb/as9.6/bin/pg_ctl -w -D /var/lib/edb/as9.6/data -l /var/lib/edb/as9.6/data/startup.log start

echo ""
echo "1.1.5 List running 'edb-post' processes"
ps -ef | grep edb-post

echo ""
echo "1.1.6 Give enterprisedb database user a password"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -c "ALTER USER enterprisedb IDENTIFIED BY enterprisedb"

echo "***********************************************************************************"
echo ""
echo ""
echo "2 Create test database and perform simulated application operations"

echo ""
echo "2.1 Create \"test\" database and users as enterprisedb database user"
echo "...... Run create_test_db_and_users.sql script"
/usr/edb/as9.6/bin/psql -p 5444 -d edb -U enterprisedb -f /var/lib/edb/as9.6/create_test_db_and_users.sql

echo ""
echo "2.2 Create test application database objects in \"test\" database as testapp_admin database user"
echo "...... Run create_app_schema.sql script"
/usr/edb/as9.6/bin/psql -p 5444 -d test -U testapp_admin -f /var/lib/edb/as9.6/create_app_schema.sql

echo ""
echo "2.3 Simulate an application (testapp1) accessing database as testapp1_dbuser database user"
echo "...... Run testapp1.sql script"
/usr/edb/as9.6/bin/psql -p 5444 -d test -U testapp1_dbuser -f /var/lib/edb/as9.6/testapp1.sql

echo ""
echo "2.4 Simulate an application (testapp2) accessing database as testapp2_dbuser database user"
echo "...... Run testapp2.sql script"
/usr/edb/as9.6/bin/psql -p 5444 -d test -U testapp1_dbuser -f /var/lib/edb/as9.6/testapp2.sql


echo "***********************************************************************************"
echo "***********************************************************************************"

#Uncomment the following to enter into docker instance for further inspection if desired
#echo ""
#echo "Opening bash prompt"
#echo "..."
#/bin/bash -l

echo ""
echo "Exiting EDB Audit Tag example entrypoint script"
echo ""




