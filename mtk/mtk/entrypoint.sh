#!/bin/bash
echo "PARAMS: $@"

if [ "$#" -lt "6" ]; then
   echo ""
   echo "USAGE: docker run <container> <source JDBC URL> <source user> <source password> <target JDBC URL> <target user> <target password> <additional MTK args>"
   echo ""
   echo "Example: docker run <container> jdbc:oracle:thin:@192.168.0.100:1521:xe hr hr jdbc:edb://192.168.0.100:5444/edb enterprisedb enterprisedb -dropSchema true hr"
   echo ""
   exit 1 
fi

sed -i "s,SRC_DB_URL=.*$,SRC_DB_URL=$1," /usr/edb/migrationtoolkit/etc/toolkit.properties
shift

sed -i "s,SRC_DB_USER=.*$,SRC_DB_USER=$1," /usr/edb/migrationtoolkit/etc/toolkit.properties
shift

sed -i "s,SRC_DB_PASSWORD=.*$,SRC_DB_PASSWORD=$1," /usr/edb/migrationtoolkit/etc/toolkit.properties
shift

sed -i "s,TARGET_DB_URL=.*$,TARGET_DB_URL=$1," /usr/edb/migrationtoolkit/etc/toolkit.properties
shift

sed -i "s,TARGET_DB_USER=.*$,TARGET_DB_USER=$1," /usr/edb/migrationtoolkit/etc/toolkit.properties
shift

sed -i "s,TARGET_DB_PASSWORD=.*$,TARGET_DB_PASSWORD=$1," /usr/edb/migrationtoolkit/etc/toolkit.properties
shift

cat /usr/edb/migrationtoolkit/etc/toolkit.properties

echo "/usr/edb/migrationtoolkit/bin/runMTK.sh $@"
/usr/edb/migrationtoolkit/bin/runMTK.sh $@
