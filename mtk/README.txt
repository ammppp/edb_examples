
Pre-requisites:
   - Docker installed and running
   - Git command line installed
   - SQL Developer installed (optional)
   - pgAdmin installed (optional)
   - Access to EDB YUM repository credentials

Demo Script:

  Big Terminal Window
   cd ~
   mdkir docker
   cd docker
   git clone https://ammppp@bitbucket.org/ammppp/edb_examples.git
   cd edb_examples
   git pull
   cd mtk
   make alias  #then enter your password
   export EDB_YUM_USERNAME=<username>
   export EDB_YUM_PASSWORD=<password>
   make build
   clear

  Small Terminal Window (for OracleDB)
   cd ~/docker/edb_examples/mtk
   make runOracle
   # Wait for Oracle to start

  Small Terminal Window (for EPAS)
   cd ~/docker/edb_examples/mtk
   make runEpas
   # Wait for EPAS to start

  SQL Developer
   # Login to 192.168.0.100, sid=xe, user=hr, password=hr, port=1521
   # Show tables and procedures

  pgAdmin
   # Login to 192.168.0.100, port=5444, user=enterprisedb, password=enterprisedb, maintenanceDB=edb
   # Show there is no hr schema

  Big Terminal Windows
   cd ~/docker/edb_examples/mtk
   make runMtk
   # Show the MTK log and not that EPAS is login as it is being loaded

  pgAdmin
   # Login to 192.168.0.100, port=5444, user=enterprisedb, password=enterprisedb, maintenanceDB=edb
   # Show the hr schema matches what was in Oracle

