/*
SELECT * FROM dba_users;
*/

PROMPT .. Unlocking HR account
ALTER USER hr ACCOUNT UNLOCK;

PROMPT .. Updating HR account password
ALTER USER hr IDENTIFIED BY hr;

/*
SELECT * FROM dba_roles;
*/

PROMPT .. Updating HR account roles to facilitate migration
GRANT dba TO hr;

PROMPT .. Updating HR account privileges to support Oracle-to-EPAS SMR replication
GRANT CREATE ANY TRIGGER TO hr;
GRANT LOCK ANY TABLE TO hr;

PROMPT .. Create ORAPUBUSER and assign privileges to support Oracle-to-EPAS SMR replication
DROP USER orapubuser;

CREATE USER orapubuser IDENTIFIED BY oracle DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp QUOTA UNLIMITED ON users;
GRANT CONNECT TO orapubuser;
GRANT RESOURCE TO orapubuser;
GRANT CREATE ANY TRIGGER TO orapubuser;
GRANT LOCK ANY TABLE TO orapubuser;

GRANT SELECT ON hr.EMPLOYEES TO orapubuser;
GRANT SELECT ON hr.LOCATIONS TO orapubuser;
GRANT SELECT ON hr.REGIONS TO orapubuser;
GRANT SELECT ON hr.JOBS TO orapubuser;
GRANT SELECT ON hr.JOB_HISTORY TO orapubuser;
GRANT SELECT ON hr.DEPARTMENTS TO orapubuser;
GRANT SELECT ON hr.COUNTRIES TO orapubuser;
GRANT SELECT ON hr.EMP_DETAILS_VIEW TO orapubuser;

PROMPT
PROMPT .. Database is started, disconnecting setup script...
PROMPT 

EXIT
