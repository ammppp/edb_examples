import java.sql.*;

public class Test 
{
  public static void main(String[] args)
  {
    try{
      System.out.println("Connecting with connection string: " + args[0]);
      Connection con = DriverManager.getConnection(args[0]);

      //create a statement object to perform DML operations
      Statement stmt = con.createStatement();
      DatabaseMetaData dbmt = con.getMetaData();
      //Queries
      String selQuery = "SELECT * from DUAL";
      //Perform a simple select query. 
      System.out.println("Performing select query:"+selQuery);
      try{
        ResultSet rs = stmt.executeQuery(selQuery);            
        ResultSetMetaData rsmd = rs.getMetaData();
        for(int i=1;i<=rsmd.getColumnCount();i++){
          System.out.print(rsmd.getColumnLabel(i)+"\t");        
        }
        System.out.println();
        for(int i=1;i<=rsmd.getColumnCount();i++){
          System.out.print("=====\t");                           
        }
        System.out.println();
        boolean found = false;
        while(rs.next()){
          found = true;
          System.out.print(rs.getString(1));
          System.out.println();
        }
        if(!found) System.out.println("No Data Found.");
      }catch(Exception exp){
        System.out.println("There was an error performing the select query.\nRoot Cause:\n");
        exp.printStackTrace();
      }
      System.out.println();     
      con.close(); 
    }catch(Exception exp){
      exp.printStackTrace();
    }
  }
}

