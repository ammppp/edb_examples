#########################################################################
# Setup all of the certificates.  Below we create a CA,
# which you may not need if you are using an external CA.
#########################################################################
mkdir ~/ca 
cd ~/ca 
 
# Create ca.conf file
cat <<EOF > ca.conf
[ ca ]
default_ca = ca_default
[ ca_default ]
dir = /var/lib/ppas/ca
certs = \$dir
new_certs_dir =\$dir/ca.db.certs
database = \$dir/ca.db.index
serial = \$dir/ca.db.serial
RANDFILE = \$dir/ca.db.rand
certificate = \$dir/ca.crt
private_key = \$dir/ca.key
default_days = 365
default_crl_days = 30
default_md = sha1
preserve = no 
policy = generic_policy
[ generic_policy ]
countryName = optional
stateOrProvinceName = optional
localityName = optional
organizationName = optional
organizationalUnitName = optional
commonName = optional
emailAddress = optional
EOF

# Create some initial files 
mkdir ca.db.certs 
touch ca.db.index 
echo "0001" > ca.db.serial  
 
# Generate the CA cert and put in keystore (the truststore)
openssl genrsa 1024 > ca.key 
openssl req -new -x509 -sha1 -key ca.key -out ca.crt -subj "/C=US/ST=Teas/L=Dallas/O=My CA/OU=My Department/CN=my-ca.com" 
openssl x509 -in ca.crt -out ca.der -outform der
keytool -import -noprompt -keystore ca.keystore -storepass "ca-key-pass" -alias castore -file ca.der

# Generate a cert for the DB server
openssl genrsa 1024 > server.key 
openssl req -new -sha1 -key server.key -out server.csr -subj "/C=US/ST=Teas/L=Dallas/O=My Company/OU=ControlDB/CN=127.0.0.1" 
openssl ca -batch -md sha1 -config ca.conf -out server.crt -infiles server.csr  

# Generate a cert and JKS for the enterprisedb user
openssl genrsa 1024 > enterprisedb.key 
openssl req -new -sha1 -key enterprisedb.key -out enterprisedb.csr -subj "/C=US/ST=Teas/L=Dallas/O=My Company/OU=My Department/CN=enterprisedb" 
openssl ca -batch -md sha1 -config ca.conf -out enterprisedb.crt -infiles enterprisedb.csr 
openssl pkcs12 -export -passout pass:enterprisedb-key-pass  -in enterprisedb.crt -inkey enterprisedb.key > enterprisedb.p12 
keytool -importkeystore -srckeystore enterprisedb.p12 -destkeystore enterprisedb.jks -srcstoretype pkcs12 -destkeypass "enterprisedb-key-pass"  -deststorepass "enterprisedb-key-pass" -srcstorepass "enterprisedb-key-pass" 

# Generate an initial CRL
openssl ca -config ca.conf -gencrl -keyfile ca.key -cert ca.crt -out ca.crl 


#######################################################################
# Setup two way SSL to be required by all IPV4 and IPV6 connections to both servers
#######################################################################
echo "hostssl all all 0.0.0.0/0 cert clientcert=1" > /var/lib/ppas/9.4/data/pg_hba.conf 

#######################################################################
# Copy CA and server certs to PGDATA dirs 
#######################################################################
cp ~/ca/ca.crt ~/ca/ca.crl ~/ca/server.key ~/ca/server.crt /var/lib/ppas/9.4/data
chmod 600 /var/lib/ppas/9.4/data/*.key 
chmod 600 /var/lib/ppas/9.4/data/*.crt 
chmod 600 /var/lib/ppas/9.4/data/*.crl

#######################################################################
# Setup both PGDATA clusters for SSL
#######################################################################
sed -i "s/#logging_collector = off/logging_collector = on/"            ~/9.4/data/postgresql.conf 
sed -i "s/#ssl = on/ssl = on/"                                         ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_ca_file = ''/ssl_ca_file = 'ca.crt'/"                   ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_crl_file = ''/ssl_crl_file = 'ca.crl'/"                 ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_cert_file = 'server.crt'/ssl_cert_file = 'server.crt'/" ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_key_file = 'server.key'/ssl_key_file = 'server.key'/"   ~/9.4/data/postgresql.conf 

#######################################################################
# Copy keys/certs to enterprisedb's home director
#######################################################################
mkdir ~/.postgresql 
cp ~/ca/enterprisedb.crt  ~/.postgresql/enterprisedb.crt 
cp ~/ca/enterprisedb.key  ~/.postgresql/enterprisedb.key 
cp ~/ca/enterprisedb.jks  ~/.postgresql/enterprisedb.jks 
cp ~/ca/enterprisedb.p12  ~/.postgresql/enterprisedb.p12 
cp ~/ca/ca.keystore       ~/.postgresql/ca.keystore 
cp ~/ca/ca.crt            ~/.postgresql/ca.crt 
chmod 600 ~/.postgresql/*

##database=edb?ssl=true&sslfactory=org.postgresql.ssl.jdbc4.LibPQFactory&sslcert=/var/lib/ppas/.postgresql/repl_user.crt&sslkey=/var/lib/ppas/.postgresql/repl_user.key&sslrootcert=/var/lib/ppas/.postgresql/ca.crt

#######################################################################
# Start the master databases 
#######################################################################
/usr/ppas-9.4/bin/pg_ctl -wD /var/lib/ppas/9.4/data start

echo "####################################################"
echo "## Connecting with psql with standard crt/key"
echo "####################################################"
export PGSSLCERT=/var/lib/ppas/.postgresql/enterprisedb.crt
export PGSSLKEY=/var/lib/ppas/.postgresql/enterprisedb.key
psql -d edb -U enterprisedb -p 5444 -h 127.0.0.1 -c "select * from DUAL"
unset PGSSLCERT PGSSLKEY

echo "####################################################"
echo "## Building Java client..."
echo "####################################################"
cd ~
javac -d . Test.java
export KEYSTORE_OPTIONS="-Djavax.net.ssl.keyStoreType=pkcs12 -Djavax.net.ssl.keyStore=/var/lib/ppas/.postgresql/enterprisedb.p12 -Djavax.net.ssl.keyStorePassword=enterprisedb-key-pass"
export TRUSTSTORE_OPTIONS="-Djavax.net.ssl.trustStoreType=jks -Djavax.net.ssl.trustStore=/var/lib/ppas/.postgresql/ca.keystore -Djavax.net.ssl.trustStorePassword=ca-key-pass"
export OPTIONS="$KEYSTORE_OPTIONS $TRUSTSTORE_OPTIONS"

echo "####################################################"
echo "## Running Java client with standard options (keystore/truststore)..."
echo "####################################################"
java $OPTIONS -cp .:/usr/ppas/connectors/jdbc/edb-jdbc17.jar Test "jdbc:edb://127.0.0.1:5444/edb?ssl=true"

echo "####################################################"
echo "## Running Java client with LibPQFactory class..."
echo "####################################################"
openssl pkcs8 -topk8 -inform PEM -outform DER -in ~/.postgresql/enterprisedb.key -out ~/.postgresql/enterprisedb.pk8 -nocrypt
java  -cp .:/usr/ppas/connectors/jdbc/edb-jdbc17.jar Test "jdbc:edb://127.0.0.1:5444/edb?ssl=true&sslmode=verify-full&&sslfactory=com.edb.ssl.jdbc4.LibPQFactory&sslcert=/var/lib/ppas/.postgresql/enterprisedb.crt&sslkey=/var/lib/ppas/.postgresql/enterprisedb.pk8&sslrootcert=/var/lib/ppas/.postgresql/ca.crt"

echo "####################################################"
echo "## Exiting with error code..."
echo "####################################################"
exit 1
 
/usr/ppas-9.4/bin/pg_ctl -mf -D /var/lib/ppas/9.4/data stop
