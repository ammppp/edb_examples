# EDB Example Running EPAS 9.6 pg_trgm on EPAS 9.4 

## Instructions

```
git clone https://bitbucket.org/ammppp/edb_examples
cd edb_examples/pg_trgm

export EDB_YUM_USERNAME=<your username>
export EDB_YUM_PASSWORD=<your password>

docker-compose build pg_trgm
docker-compose run pg_trgm
```
