#!/bin/bash -l
echo ""
echo "***********************************************************************************"
echo "***********************************************************************************"
echo ""
echo "Running ppas94 entrypoint script"
echo "..."

cd ~/9.4

echo ""
echo "***********************************************************************************"
echo ""
echo "#########################################################################"
echo "# Setup all of the certificates.  Below we create a CA,"
echo "# which you may not need if you are using an external CA."
echo "#########################################################################"
mkdir ~/ca 
cd ~/ca 

# Create ca.conf file
cat <<EOF > ca.conf
[ ca ]
default_ca = ca_default
[ ca_default ]
dir = /var/lib/ppas/ca
certs = \$dir
new_certs_dir =\$dir/ca.db.certs
database = \$dir/ca.db.index
serial = \$dir/ca.db.serial
RANDFILE = \$dir/ca.db.rand
certificate = \$dir/ca.crt
private_key = \$dir/ca.key
default_days = 365
default_crl_days = 30
default_md = sha1
preserve = no 
policy = generic_policy
[ generic_policy ]
countryName = optional
stateOrProvinceName = optional
localityName = optional
organizationName = optional
organizationalUnitName = optional
commonName = optional
emailAddress = optional
EOF

echo "# Create some initial files"
mkdir ca.db.certs 
touch ca.db.index 
echo "0001" > ca.db.serial  
 
echo "# Generate the CA cert and put in keystore (the truststore)"
openssl genrsa 1024 > ca.key 
openssl req -new -x509 -sha1 -key ca.key -out ca.crt -subj "/C=US/ST=Virginia/L=Manassas/O=My CA/OU=My Department/CN=my-ca.com" 
openssl x509 -in ca.crt -out ca.der -outform der
keytool -import -noprompt -keystore ca.keystore -storepass "ca-key-pass" -alias castore -file ca.der

echo "# Generate a cert for the DB server"
openssl genrsa 1024 > data.key 
openssl req -new -sha1 -key data.key -out data.csr -subj "/C=US/ST=Virginia/L=Manassas/O=My Company/OU=Data/CN=localhost" 
openssl ca -batch -md sha1 -config ca.conf -out data.crt -infiles data.csr

echo "# Generate a cert and JKS for the enterprisedb user"
openssl genrsa 1024 > enterprisedb.key 
openssl req -new -sha1 -key enterprisedb.key -out enterprisedb.csr -subj "/C=US/ST=Virginia/L=Manassas/O=My Company/OU=My Department/CN=enterprisedb" 
openssl ca -batch -md sha1 -config ca.conf -out enterprisedb.crt -infiles enterprisedb.csr 
openssl pkcs12 -export -passout pass:enterprisedb-key-pass  -in enterprisedb.crt -inkey enterprisedb.key > enterprisedb.p12 
keytool -importkeystore -srckeystore enterprisedb.p12 -destkeystore enterprisedb.jks -srcstoretype pkcs12 -destkeypass "enterprisedb-key-pass"  -deststorepass "enterprisedb-key-pass" -srcstorepass "enterprisedb-key-pass" 

echo "# Generate a cert and JKS for a repl_user"
openssl genrsa 1024 > repl_user.key 
openssl req -new -sha1 -key repl_user.key -out repl_user.csr -subj "/C=US/ST=Virginia/L=Manassas/O=My Company/OU=My Department/CN=repl_user" 
openssl ca -batch -md sha1 -config ca.conf -out repl_user.crt -infiles repl_user.csr 
openssl pkcs12 -export -passout pass:repl-key-pass  -in repl_user.crt -inkey repl_user.key > repl_user.p12 
keytool -importkeystore -srckeystore repl_user.p12 -destkeystore repl_user.jks -srcstoretype pkcs12 -destkeypass "repl-key-pass"  -deststorepass "repl-key-pass" -srcstorepass "repl-key-pass" 

echo "# Generate an initial CRL"
openssl ca -config ca.conf -gencrl -keyfile ca.key -cert ca.crt -out ca.crl 

echo "#######################################################################"
echo "# Create a cluster, data on 5444"
echo "#"
echo "#######################################################################"
/usr/ppas-9.4/bin/initdb -D /var/lib/ppas/9.4/data 

echo "#######################################################################"
echo "# Setup two way SSL to be required by all IPV4 and IPV6 connections to both servers"
echo "#######################################################################"
echo "hostssl all all 0.0.0.0/0 cert clientcert=1" > /var/lib/ppas/9.4/data/pg_hba.conf 
echo "hostssl all all ::0/0 cert clientcert=1" >> /var/lib/ppas/9.4/data/pg_hba.conf

echo "#######################################################################"
echo "# Configure the necessary settings to permit the clusters to stream logs to the Replication Server"
echo "#######################################################################"
#sed -i 's/#wal_level = minimal/wal_level = logical/' /var/lib/ppas/9.4/data/postgresql.conf 
#sed -i 's/#max_wal_senders = 0/max_wal_senders = 5/' /var/lib/ppas/9.4/data/postgresql.conf 
#sed -i 's/#max_replication_slots = 0/max_replication_slots = 5/' /var/lib/ppas/9.4/data/postgresql.conf 
#sed -i 's/#track_commit_timestamp = off/track_commit_timestamp = on/' /var/lib/ppas/9.4/data/postgresql.conf 
#sed -i 's/#host[ ]*replication/host replication/g' /var/lib/ppas/9.4/data/pg_hba.conf

echo "#######################################################################"
echo "# Copy CA and server certs to PGDATA dirs "
echo "#######################################################################"
cp ~/ca/ca.crt ~/ca/ca.crl ~/ca/data.key ~/ca/data.crt /var/lib/ppas/9.4/data
chmod 600 /var/lib/ppas/9.4/data/*.key 
chmod 600 /var/lib/ppas/9.4/data/*.crt 
chmod 600 /var/lib/ppas/9.4/data/*.crl

echo "#######################################################################"
echo "# Setup both PGDATA clusters for SSL"
echo "#######################################################################"
sed -i "s/#logging_collector = off/logging_collector = on/"               ~/9.4/data/postgresql.conf 
sed -i "s/#ssl = off/ssl = on/"                                           ~/9.4/data/postgresql.conf 
sed -i "s/#ssl = on/ssl = on/"                                           ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_ca_file = ''/ssl_ca_file = 'ca.crt'/"                      ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_crl_file = ''/ssl_crl_file = 'ca.crl'/"                    ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_ciphers = 'HIGH:MEDIUM:+3DES:!aNULL'/#ssl_ciphers = 'TLSv1.2'/"     ~/9.4/data/postgresql.conf

sed -i "s/#ssl_cert_file = 'server.crt'/ssl_cert_file = 'data.crt'/" ~/9.4/data/postgresql.conf 
sed -i "s/#ssl_key_file = 'server.key'/ssl_key_file = 'data.key'/"   ~/9.4/data/postgresql.conf 

echo "#######################################################################"
echo "# Copy keys/certs to enterprisedb's home director"
echo "#######################################################################"
mkdir ~/.postgresql 
cp ~/ca/enterprisedb.crt  ~/.postgresql/postgresql.crt 
cp ~/ca/enterprisedb.key  ~/.postgresql/postgresql.key 
cp ~/ca/enterprisedb.jks  ~/.postgresql/enterprisedb.jks 
cp ~/ca/ca.keystore       ~/.postgresql/ca.keystore 
cp ~/ca/enterprisedb.p12  ~/.postgresql/enterprisedb.p12 
cp ~/ca/repl_user.p12     ~/.postgresql/repl_user.p12 
cp ~/ca/repl_user.crt     ~/.postgresql/repl_user.crt 
cp ~/ca/repl_user.key     ~/.postgresql/repl_user.key 
chmod 600 ~/.postgresql/*

echo "#######################################################################"
echo "# Start the master databases "
echo "#######################################################################"
/usr/ppas-9.4/bin/pg_ctl -wD /var/lib/ppas/9.4/data start

echo "#######################################################################"
echo "# Create the repl_user that will by used by xDB"
echo "#######################################################################"
psql -d edb -U enterprisedb -p 5444 -h localhost -c "create user repl_user"
psql -d edb -U enterprisedb -p 5444 -h localhost -c "alter user repl_user with superuser"

echo "#######################################################################"
echo "# Create three tables to be replicated on each cluster"
echo "#######################################################################"
export PGSSLCERT=~/.postgresql/repl_user.crt
export PGSSLKEY=~/.postgresql/repl_user.key

psql -d edb -U repl_user -p 5444 -h localhost -c "create table public.table1 (id integer primary key)"
psql -d edb -U repl_user -p 5444 -h localhost -c "create table public.table2 (id text primary key)"
psql -d edb -U repl_user -p 5444 -h localhost -c "create table public.table3 (id integer primary key)"

psql -d edb -U repl_user -p 5444 -h localhost -c "insert into public.table1 values ( 1 )"

unset PGSSLCERT PGSSLKEY

#echo "#######################################################################"
#echo "# Stop the EPAS cluster"
#echo "#######################################################################"
#/usr/ppas-9.4/bin/pg_ctl -mf -D /var/lib/ppas/9.4/data stop


echo "***********************************************************************************"
echo "***********************************************************************************"

#Uncomment the following to enter into docker instance for further inspection if desired
echo ""
echo "Opening bash prompt"
echo "..."
/bin/bash -l

echo ""
echo "Exiting ppas94 entrypoint script"
echo ""




